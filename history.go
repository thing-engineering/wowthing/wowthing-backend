package main

import (
	"fmt"
	// "strconv"
	"strings"
	"time"
)

const (
	HISTORY_INTERVAL    = 10
	HISTORY_TIME_FORMAT = "2006-01-02 15:04"
)

func HistoryWorker() {
	log.Info("HistoryWorker started")

	// Work out how long to wait for initially
	now := time.Now()
	min := (60 - now.Minute() - 1) % HISTORY_INTERVAL
	sec := (60 - now.Second() - 1)
	nano := 1000000000 - now.Nanosecond()

	if min < 0 {
		min = 9
	}
	if sec < 0 {
		sec = 59
	}

	sleepFor := (time.Duration(min) * time.Minute) + (time.Duration(sec) * time.Second) + (time.Duration(nano) * time.Nanosecond)
	log.Debugf("HistoryWorker sleeping for %s", sleepFor)
	time.Sleep(sleepFor)

	HistoryUpdate(time.Now())

	for t := range time.Tick(time.Duration(HISTORY_INTERVAL) * time.Minute) {
		HistoryUpdate(t)
	}
}

func HistoryUpdate(t time.Time) {
	timeStr := t.Format(HISTORY_TIME_FORMAT)
	historyMap := make(map[int]map[int]int64)

	// Fetch the most recent history data for each account/realm
	rows, err := db.Query(`
SELECT bnetaccount_id,
       realm_id,
       gold
FROM (
	SELECT bnetaccount_id,
           realm_id,
           gold,
           time,
           MAX(time) OVER (PARTITION BY bnetaccount_id, realm_id) max_time
    FROM thing_goldhistory
) t1
WHERE time = max_time
`)
	if err != nil {
		LogError(err, "HistoryUpdate")
		return
	}
	defer rows.Close()

	var bnetaccountID, realmID int
	var gold int64
	for rows.Next() {
		if err := rows.Scan(&bnetaccountID, &realmID, &gold); err != nil {
			LogError(err, "HistoryUpdate")
			return
		}

		if _, ok := historyMap[bnetaccountID]; !ok {
			historyMap[bnetaccountID] = make(map[int]int64)
		}
		historyMap[bnetaccountID][realmID] = gold
	}

	// Fetch current gold data
	goldRows, err := db.Query(`
SELECT bnetaccount_id,
       realm_id,
       ROUND(SUM(copper) / 10000) AS gold
FROM thing_character
WHERE bnetaccount_id IS NOT NULL
GROUP BY bnetaccount_id,
         realm_id
`)
	if err != nil {
		LogError(err, "HistoryUpdate")
		return
	}
	defer goldRows.Close()

	var valueStrings []string
	for goldRows.Next() {
		if err := goldRows.Scan(&bnetaccountID, &realmID, &gold); err != nil {
			LogError(err, "HistoryUpdate")
			return
		}

		oldGold, ok := historyMap[bnetaccountID][realmID]
		// if ok {
		// 	if gold != oldGold {
		// 		log.Debug("gold changed")
		// 	} else {
		// 		log.Debug("gold same")
		// 	}
		// } else {
		if !ok || (ok && gold != oldGold) {
			valueStrings = append(valueStrings, fmt.Sprintf("(%d, %d, %d, '%s')", bnetaccountID, realmID, gold, timeStr))
		}

		// log.Debug("%d %d %d", bnetaccountID, realmID, gold)

		// log.Debug("%d", historyMap[bnetaccountID][realmID])
	}

	// Insert them all
	if len(valueStrings) > 0 {
		_, err := db.Exec(fmt.Sprintf(`
INSERT INTO thing_goldhistory (bnetaccount_id, realm_id, gold, time)
VALUES %s
`, strings.Join(valueStrings, ", ")))
		if err != nil {
			LogError(err, "HistoryUpdate")
			return
		}
	}

	log.Debugf("HistoryUpdate update took %s", time.Since(t))
}
