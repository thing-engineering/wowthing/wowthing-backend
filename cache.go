package main

import (
	"encoding/json"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"io/ioutil"
	"strconv"
	"strings"
	"time"
)

const (
	MAX_LEVEL = 120
)

var (
	XP_PER_LEVEL = [...]int{
		0, 400, 900, 1400, 2100, 2800, 3800, 5000, 6400, 8100, // 1-10
		9240, 10780, 13230, 16800, 20380, 24440, 28080, 31500, 34800, 38550, // 11-20
		42320, 46560, 49440, 52000, 55040, 58400, 61120, 64160, 66880, 71680, // 21-30
		76160, 81440, 85600, 90240, 94560, 99200, 104160, 108480, 113280, 117920, // 31-40
		123280, 128750, 134160, 139870, 145490, 151440, 157500, 163470, 169760, 176180, // 41-50
		182480, 189130, 195900, 202540, 209540, 216660, 223640, 230990, 238460, 245790, // 51-60
		254300, 261860, 269810, 277880, 285780, 294080, 302500, 311040, 319400, 328170, // 61-70
		337060, 345750, 354880, 364120, 373150, 382630, 392220, 401600, 411430, 421370, // 71-80
		431440, 441270, 451560, 461980, 472150, 482800, 493570, 504070, 515080, 526200, // 81-90
		542590, 554010, 565540, 576780, 588550, 600440, 612440, 624560, 636810, 648730, // 91-100
		657000, 663000, 669000, 675000, 681000, 687000, 693000, 699000, 705000, 711000, // 101-110
		717000, 831450, 838350, 845250, 852150, 859050, 865950, 872850, 879750, 886650, // 111-120
	}
)

type AchievementData struct {
	FactionID int `json:factionId`
	Id        int `json:id`
	Points    int `json:points`
}
type CategoryData struct {
	Achievements []AchievementData `json:achievements`
	Categories   []CategoryData    `json:categories`
	Name         string            `json:name`
}
type Achievements struct {
	Achievements []CategoryData
}

type Leaderboard struct {
	BNetAccountID int
	Changed       bool

	AchievementTotal    int
	AchievementAlliance int
	AchievementHorde    int
	AchievementFeat     int
	HonorableKillTotal  int
	MountUnique         int
	PetUnique           int
	PetScore            int
	ReputationExalted   int
	ToyUnique           int
	XPTotal             int64
}

type PetDataJson struct {
	PetId     int    `json:"i"`
	Favourite bool   `json:"f"`
	Level     int    `json:"l"`
	Name      string `json:"n"`
	Quality   int    `json:"q"`
}

type RedisTimes struct {
	Achievements int64 `redis:"achievements"`
	Criteria     int64 `redis:"criteria"`
	Mounts       int64 `redis:"mounts"`
	Pets         int64 `redis:"pets"`
	Quests       int64 `redis:"quests"`
	Reputations  int64 `redis:"reputations"`
}

var achievementDataMap map[int]AchievementData
var achievementFeatMap map[int]interface{}
var criteriaAchievementMap map[string][]int
var leaderboardMap map[int]*Leaderboard
var xpTotalForLevel map[int]map[int]int64

func Cacher() {
	log.Info("Cacher started")

	// Load the criteria:[achievements] map
	data, err := ioutil.ReadFile("criteria_map.json")
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(data, &criteriaAchievementMap)
	if err != nil {
		panic(err)
	}

	// Load achievement data
	achievementDataMap = make(map[int]AchievementData)
	achievementFeatMap = make(map[int]interface{})
	LoadAchievementData()

	// Pre-calculate XP per level
	xpTotalForLevel = make(map[int]map[int]int64)
	xpTotalForLevel[0] = make(map[int]int64)
	xpTotalForLevel[6] = make(map[int]int64)
	xpTotalForLevel[12] = make(map[int]int64)
	for i := 1; i <= MAX_LEVEL; i++ {
		xpTotalForLevel[0][i] = 0
		for j := 1; j <= i; j++ {
			xpTotalForLevel[0][i] += int64(XP_PER_LEVEL[j-1])
		}

		// Death Knights start at 56
		xpTotalForLevel[6][i] = 0
		for j := 56; j <= i; j++ {
			xpTotalForLevel[6][i] += int64(XP_PER_LEVEL[j-1])
		}

		// Demon Hunters start at 101
		xpTotalForLevel[12][i] = 0
		for j := 101; j <= i; j++ {
			xpTotalForLevel[12][i] += int64(XP_PER_LEVEL[j-1])
		}
	}

	// Looooop
	for _ = range time.Tick(time.Duration(Config.General.CacheInterval) * time.Minute) {
		start := time.Now()

		// Fetch account IDs
		rows, err := db.Query(`
SELECT id
FROM core_bnetaccount
`)
		if err != nil {
			panic(err)
		}
		defer rows.Close()

		var accountID int
		var accountIDs []int
		for rows.Next() {
			if err := rows.Scan(&accountID); err != nil {
				panic(err)
			}
			accountIDs = append(accountIDs, accountID)
		}

		// Fetch leaderboard data
		leaderboardMap = FetchLeaderboardData()

		// Update and cache data
		for _, accountID := range accountIDs {
			cached := GetRedisTimes(fmt.Sprintf("cached:%d", accountID))
			updated := GetRedisTimes(fmt.Sprintf("updated:%d", accountID))

			if _, ok := leaderboardMap[accountID]; !ok {
				leaderboardMap[accountID] = &Leaderboard{}
			}

			if updated.Achievements >= cached.Achievements || updated.Criteria >= cached.Criteria {
				UpdateAchievements(accountID)
			}

			if updated.Mounts >= cached.Mounts {
				UpdateMounts(accountID)
			}

			if updated.Pets >= cached.Pets {
				UpdatePets(accountID)
			}

			if updated.Quests >= cached.Quests {
				UpdateQuests(accountID)
			}

			if updated.Reputations >= cached.Reputations {
				UpdateReputation(accountID)
			}

			UpdateToys(accountID)
			UpdateXPTotal(accountID)
		}

		// Update leaderboard data
		UpdateLeaderboard()

		log.Debugf("Leaderboard update took %s", time.Since(start))
	}
}

func GetRedisTimes(key string) RedisTimes {
	// Get a Redis connection
	conn := redisPool.Get()
	defer conn.Close()

	var cached RedisTimes

	values, err := redis.Values(conn.Do("HGETALL", key))
	if err != nil {
		LogError(err, "GetRedisTimes")
		return cached
	}

	if err := redis.ScanStruct(values, &cached); err != nil {
		LogError(err, "GetRedisTimes")
		return cached
	}

	return cached
}

func FetchLeaderboardData() map[int]*Leaderboard {
	leaderboardMap := make(map[int]*Leaderboard)

	rows, err := db.Query(`
SELECT  bnetaccount_id,
        achievement_total,
        achievement_alliance,
        achievement_horde,
        achievement_feat,
        honorable_kill_total,
        mount_unique,
        pet_unique,
        pet_score,
        reputation_exalted,
        toy_unique,
        xp_total
FROM    thing_leaderboard
`)
	defer rows.Close()

	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var lb Leaderboard
		if err := rows.Scan(&lb.BNetAccountID, &lb.AchievementTotal, &lb.AchievementAlliance, &lb.AchievementHorde, &lb.AchievementFeat,
			&lb.HonorableKillTotal, &lb.MountUnique, &lb.PetUnique, &lb.PetScore, &lb.ReputationExalted, &lb.ToyUnique, &lb.XPTotal); err != nil {
			panic(err)
		}
		leaderboardMap[lb.BNetAccountID] = &lb
	}

	return leaderboardMap
}

func UpdateLeaderboard() {
	var parts []string
	for accountID, leaderboard := range leaderboardMap {
		if leaderboard.Changed {
			parts = append(parts, fmt.Sprintf("(%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d)",
				accountID,
				leaderboard.AchievementTotal,
				leaderboard.AchievementAlliance,
				leaderboard.AchievementHorde,
				leaderboard.AchievementFeat,
				leaderboard.HonorableKillTotal,
				leaderboard.MountUnique,
				leaderboard.PetUnique,
				leaderboard.PetScore,
				leaderboard.ReputationExalted,
				leaderboard.ToyUnique,
				leaderboard.XPTotal))
		}
	}

	if len(parts) == 0 {
		return
	}

	_, err := db.Exec(fmt.Sprintf(`
UPDATE thing_leaderboard AS lb
SET achievement_total = new.achievement_total,
    achievement_alliance = new.achievement_alliance,
    achievement_horde = new.achievement_horde,
    achievement_feat = new.achievement_feat,
    honorable_kill_total = new.honorable_kills,
    mount_unique = new.mount_unique,
    pet_unique = new.pet_unique,
    pet_score = new.pet_score,
    reputation_exalted = new.reputation_exalted,
    toy_unique = new.toy_unique,
    xp_total = new.xp_total
FROM (
    VALUES
    %s
) AS new(
    bnetaccount_id,
    achievement_total,
    achievement_alliance,
    achievement_horde,
    achievement_feat,
    honorable_kills,
    mount_unique,
    pet_unique,
    pet_score,
    reputation_exalted,
    toy_unique,
    xp_total
)
WHERE lb.bnetaccount_id = new.bnetaccount_id
`, strings.Join(parts, ", ")))
	if err != nil {
		panic(err)
	}
}

func LoadAchievementData() {
	var achievementData *Achievements

	// Load the criteria:[achievements] map
	data, err := ioutil.ReadFile("achievement.json")
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(data, &achievementData)
	if err != nil {
		panic(err)
	}

	// Load the points data
	for _, cat := range achievementData.Achievements {
		LoadAchievementPoints(&cat, nil)
	}
}

func LoadAchievementPoints(cat *CategoryData, parentCat *CategoryData) {
	for _, ach := range cat.Achievements {
		achievementDataMap[ach.Id] = ach

		if cat.Name == "Feats of Strength" || (parentCat != nil && parentCat.Name == "Feats of Strength") {
			achievementFeatMap[ach.Id] = nil
		}
	}

	for _, newCat := range cat.Categories {
		LoadAchievementPoints(&newCat, cat)
	}
}

func UpdateAchievements(accountID int) {
	leaderboard := leaderboardMap[accountID]

	// Get a Redis connection
	conn := redisPool.Get()
	defer conn.Close()

	origAchievementTotal := leaderboard.AchievementTotal
	origAchievementAlliance := leaderboard.AchievementAlliance
	origAchievementHorde := leaderboard.AchievementHorde
	origAchievementFeat := leaderboard.AchievementFeat

	leaderboard.AchievementTotal = 0
	leaderboard.AchievementAlliance = 0
	leaderboard.AchievementHorde = 0
	leaderboard.AchievementFeat = 0

	// Fetch achievement data for this account
	rows, err := db.Query(`
WITH blah AS (
	SELECT UNNEST(cd.achievements) AS achievement_id,
       	   UNNEST(cd.achievements_timestamp) AS timestamp
	FROM thing_characterdata cd,
         thing_character c
	WHERE cd.character_id = c.id
	      AND c.bnetaccount_id = $1
)
SELECT achievement_id, MIN(timestamp)
FROM blah
GROUP BY achievement_id
`, accountID)
	if err != nil {
		LogError(err, "UpdateAchievements")
		return
	}
	defer rows.Close()

	// Stick it in a map
	achievementMap := make(map[string]int64)

	var achievementID string
	var timestamp int64
	for rows.Next() {
		if err := rows.Scan(&achievementID, &timestamp); err != nil {
			LogError(err, "UpdateAchievements")
			return
		}
		achievementMap[achievementID] = timestamp
	}

	// Calculate total points
	for k := range achievementMap {
		sigh, _ := strconv.Atoi(k)
		var data = achievementDataMap[sigh]
		leaderboard.AchievementTotal += data.Points

		if data.FactionID == 0 || data.FactionID == 2 {
			leaderboard.AchievementAlliance += data.Points
		}
		if data.FactionID == 1 || data.FactionID == 2 {
			leaderboard.AchievementHorde += data.Points
		}

		if _, ok := achievementFeatMap[sigh]; ok {
			leaderboard.AchievementFeat++
		}
	}

	// Turn it into JSON
	b, err := json.Marshal(achievementMap)
	if err != nil {
		LogError(err, "UpdateAchievements")
		return
	}
	data := string(b)

	// Cache it
	conn.Do("SET", fmt.Sprintf("achievements:%d", accountID), data)

	SetLastCached(accountID, "achievements")

	if origAchievementTotal != leaderboard.AchievementTotal || origAchievementAlliance != leaderboard.AchievementAlliance ||
		origAchievementHorde != leaderboard.AchievementHorde || origAchievementFeat != leaderboard.AchievementFeat {
		leaderboard.Changed = true
	}

	// Fetch criteria data for this account
	rows, err = db.Query(`
SELECT cd.character_id,
       ARRAY_TO_JSON(cd.criteria),
       ARRAY_TO_JSON(cd.criteria_quantity)
FROM thing_characterdata cd,
     thing_character c
WHERE cd.character_id = c.id
      AND c.bnetaccount_id = $1
`, accountID)
	if err != nil {
		LogError(err, "UpdateAchievements")
		return
	}
	defer rows.Close()

	// Stick it in a map
	criteriaMap := make(map[string][][]int64)

	var characterID int64
	var criteriaJSON, criteriaQuantityJSON []byte
	for rows.Next() {
		var criteria []int
		var criteriaQuantity []int64

		if err := rows.Scan(&characterID, &criteriaJSON, &criteriaQuantityJSON); err != nil {
			LogError(err, "UpdateAchievements")
			return
		}

		// Sanity checks
		if len(criteriaJSON) == 0 || len(criteriaQuantityJSON) == 0 {
			continue
		}

		// Unmarshal the JSON
		if err = json.Unmarshal(criteriaJSON, &criteria); err != nil {
			LogError(err, "UpdateAchievements")
			continue
		}
		if err = json.Unmarshal(criteriaQuantityJSON, &criteriaQuantity); err != nil {
			LogError(err, "UpdateAchievements")
			continue
		}

		for i, criteriaID := range criteria {
			s := strconv.Itoa(criteriaID)

			notFound := false
			achievementIDs, ok := criteriaAchievementMap[s]
			if ok {
				for _, achievementID := range achievementIDs {
					s2 := strconv.Itoa(achievementID)
					if _, ok = achievementMap[s2]; !ok {
						notFound = true
						break
					}
				}
			}

			if notFound {
				criteriaMap[s] = append(criteriaMap[s], []int64{characterID, criteriaQuantity[i]})
			}
		}
	}

	// Turn it into JSON
	b, err = json.Marshal(criteriaMap)
	if err != nil {
		LogError(err, "UpdateAchievements")
		return
	}
	data = string(b)

	// Cache it
	conn.Do("SET", fmt.Sprintf("criteria:%d", accountID), data)

	SetLastCached(accountID, "criteria")
}

func UpdateMounts(accountID int) {
	leaderboard := leaderboardMap[accountID]

	// Get a Redis connection
	conn := redisPool.Get()
	defer conn.Close()

	// Fetch mount data for this account
	rows, err := db.Query(`
SELECT DISTINCT(UNNEST(ARRAY_CAT(cd.mounts, cd.mounts_extra)))
FROM thing_characterdata cd,
     thing_character c
WHERE cd.character_id = c.id
      AND bnetaccount_id = $1
`, accountID)
	if err != nil {
		LogError(err, "UpdateMounts")
		return
	}
	defer rows.Close()

	// Stick it in an array
	var mountID int
	var mountIDs []int
	for rows.Next() {
		if err := rows.Scan(&mountID); err != nil {
			panic(err)
		}
		mountIDs = append(mountIDs, mountID)
	}

	if len(mountIDs) != leaderboard.MountUnique {
		leaderboard.Changed = true
		leaderboard.MountUnique = len(mountIDs)
	}

	// Turn it into JSON
	b, err := json.Marshal(mountIDs)
	if err != nil {
		LogError(err, "UpdateMounts")
		return
	}
	data := string(b)

	// Cache it
	conn.Do("SET", fmt.Sprintf("mounts:%d", accountID), data)

	SetLastCached(accountID, "mounts")
}

func UpdatePets(accountID int) {
	leaderboard := leaderboardMap[accountID]

	// Get a Redis connection
	conn := redisPool.Get()
	defer conn.Close()

	origPetScore := leaderboard.PetScore
	origPetUnique := leaderboard.PetUnique

	leaderboard.PetScore = 0
	leaderboard.PetUnique = 0

	// Fetch pet data for this account  <3 Postgres
	rows, err := db.Query(`
SELECT DISTINCT ON (pet_id)
       pet_id, level, quality, favourite, name
FROM   thing_accountpet
WHERE  bnetaccount_id = $1
ORDER  BY pet_id, quality DESC, level DESC, favourite DESC
`, accountID)
	if err != nil {
		LogError(err, "UpdatePets")
		return
	}

	// Stick it in an array
	var pets []PetDataJson
	for rows.Next() {
		petData := PetDataJson{}
		if err := rows.Scan(&petData.PetId, &petData.Level, &petData.Quality, &petData.Favourite, &petData.Name); err != nil {
			LogError(err, "UpdatePets")
			return
		}
		pets = append(pets, petData)

		// Add some score - http://www.warcraftpets.com/help/#petscore
		leaderboard.PetScore += (200 + (4 * petData.Level) + (petData.Quality * 100))
		leaderboard.PetUnique++
	}

	// Turn it into JSON
	b, err := json.Marshal(pets)
	if err != nil {
		LogError(err, "UpdatePets")
		return
	}
	data := string(b)

	// Cache it
	cacheKey := fmt.Sprintf("pets:%d", accountID)
	conn.Do("SET", cacheKey, data)

	SetLastCached(accountID, "pets")

	if origPetScore != leaderboard.PetScore || origPetUnique != leaderboard.PetUnique {
		leaderboard.Changed = true
	}
}

func UpdateQuests(accountID int) {
	// Get a Redis connection
	conn := redisPool.Get()
	defer conn.Close()

	// Fetch quest data for this account
	rows, err := db.Query(`
SELECT DISTINCT(UNNEST(cd.quests)) AS questID
FROM thing_characterdata cd,
     thing_character c
WHERE cd.character_id = c.id
      AND bnetaccount_id = $1
ORDER BY questID
`, accountID)
	if err != nil {
		LogError(err, "UpdateQuests")
		return
	}

	// Stick it in an array
	var questID int
	var questIDs []int
	for rows.Next() {
		if err := rows.Scan(&questID); err != nil {
			LogError(err, "UpdateQuests")
			return
		}
		questIDs = append(questIDs, questID)
	}

	// Turn it into JSON
	b, err := json.Marshal(questIDs)
	if err != nil {
		LogError(err, "UpdateQuests")
		return
	}
	data := string(b)

	// Cache it
	conn.Do("SET", fmt.Sprintf("quests:%d", accountID), data)

	SetLastCached(accountID, "quests")
}

func UpdateReputation(accountID int) {
	leaderboard := leaderboardMap[accountID]

	origReputationExalted := leaderboard.ReputationExalted

	err := db.QueryRow(`
SELECT  COALESCE(MAX(exalted), 0)
FROM    (
    SELECT  cr.character_id, COUNT(cr.*) AS exalted
    FROM    thing_characterreputation cr,
            thing_character c
    WHERE   c.bnetaccount_id = $1
            AND c.id = cr.character_id
            AND cr.level = 7
    GROUP BY cr.character_id
) foo
`, accountID).Scan(&leaderboard.ReputationExalted)
	if err != nil {
		LogError(err, "UpdateReputation")
		return
	}

	SetLastCached(accountID, "reputations")

	if leaderboard.ReputationExalted != origReputationExalted {
		leaderboard.Changed = true
	}
}

func UpdateToys(accountID int) {
	leaderboard := leaderboardMap[accountID]

	origToyUnique := leaderboard.ToyUnique

	err := db.QueryRow(`
SELECT COUNT(DISTINCT toyID)
FROM (
    SELECT UNNEST(toys) AS toyID
    FROM core_bnetaccount
    WHERE id = $1
) temp
`, accountID).Scan(&leaderboard.ToyUnique)
	if err != nil {
		LogError(err, "UpdateToys")
		return
	}

	if leaderboard.ToyUnique != origToyUnique {
		leaderboard.Changed = true
	}
}

func UpdateXPTotal(accountID int) {
	leaderboard := leaderboardMap[accountID]

	// Fetch character data for this account
	rows, err := db.Query(`
SELECT cls_id, level, current_xp, honorable_kills
FROM   thing_character
WHERE  bnetaccount_id = $1
`, accountID)
	if err != nil {
		LogError(err, "UpdateXPTotal")
		return
	}

	origHonorableKillTotal := leaderboard.HonorableKillTotal
	origXPTotal := leaderboard.XPTotal

	leaderboard.HonorableKillTotal = 0
	leaderboard.XPTotal = 0

	// Calculate a total
	var classID, level, currentXP, honorableKills int
	for rows.Next() {
		if err := rows.Scan(&classID, &level, &currentXP, &honorableKills); err != nil {
			LogError(err, "UpdateXPTotal")
			return
		}

		xpTable, ok := xpTotalForLevel[classID]
		if !ok {
			xpTable = xpTotalForLevel[0]
		}

		leaderboard.XPTotal += xpTable[MinInt(MAX_LEVEL, level)]
		if level < MAX_LEVEL {
			leaderboard.XPTotal += int64(MinInt(XP_PER_LEVEL[level], currentXP))
		}

		// Stick this in here too
		leaderboard.HonorableKillTotal += honorableKills
	}

	if leaderboard.HonorableKillTotal != origHonorableKillTotal || leaderboard.XPTotal != origXPTotal {
		leaderboard.Changed = true
	}
}

func SetLastCached(accountID int, subKey string) {
	// Get a Redis connection
	conn := redisPool.Get()
	defer conn.Close()

	conn.Do("HSET", fmt.Sprintf("cached:%d", accountID), subKey, fmt.Sprintf("%v", time.Now().UTC().Unix()))
}
