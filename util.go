package main

import (
	"fmt"
	"path/filepath"
	"runtime"
	"strings"
)

func LogError(err error, format string, args ...interface{}) {
	formatted := fmt.Sprintf(format, args...)
	_, file, line, _ := runtime.Caller(1)
	log.Errorf("%s  [\033[33;1m%s\033[0m:\033[33m%d\033[0m] \033[31;1m%s\033[0m", formatted, filepath.Base(file), line, err.Error())
}

func LogErrorN(err error, stackCount int, format string, args ...interface{}) {
	formatted := fmt.Sprintf(format, args...)
	_, file, line, _ := runtime.Caller(stackCount)
	log.Errorf("%s  [\033[33;1m%s\033[0m:\033[33m%d\033[0m] \033[31;1m%s\033[0m", formatted, filepath.Base(file), line, err.Error())
}

// Stupid bullshit lack of generics
func MaxInt(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}
func MinInt(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

// Insert any new items into the database so that ItemWorkers can go fetch them
func InsertItems(itemIDs []int) error {
	// Oh dear
	if len(itemIDs) > 0 {
		var parts []string
		for _, id := range itemIDs {
			parts = append(parts, fmt.Sprintf("(%d, '###UPDATE###', '', '', 0, 0, 0, '{}'::integer[], 0, 0, 0, 0, '{}'::integer[], 0)", id))
		}

		// Begin transaction
		tx, err := db.Begin()
		if err != nil {
			return err
		}

		// Lock item table
		_, err = tx.Exec("LOCK TABLE core_item")
		if err != nil {
			return err
		}

		// Insert
		_, err = tx.Exec(fmt.Sprintf(`
WITH data(id, name, lname, icon, bind_type, cls, level, level_scaling, quality, sell_price, slot, subcls, upgrades, item_set) AS (
    VALUES
        %s
)
INSERT INTO core_item (id, name, lname, icon, bind_type, cls, level, level_scaling, quality, sell_price, slot, subcls, upgrades, item_set)
SELECT DISTINCT d.id, d.name, d.lname, d.icon, d.bind_type, d.cls, d.level, d.level_scaling, d.quality, d.sell_price, d.slot, d.subcls, d.upgrades, d.item_set
FROM data d
WHERE NOT EXISTS (
    SELECT 1
    FROM core_item i2
    WHERE i2.id = d.id
)
`, strings.Join(parts, ", ")))
		if err != nil {
			return err
		}

		// Commit transaction
		if err = tx.Commit(); err != nil {
			return err
		}
	}

	return nil
}
