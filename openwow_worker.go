package main

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

const (
	OPENWOW_DATA_URL = "https://api.openwow.io/v1/data/enUS/Live/%s"
)

var (
	FACTIONS = [...]string{"alliance", "horde", "neutral"}
)

type OpenwowWorker struct {
}

type OpenwowIcon struct {
	Hash string
}

type OpenwowClass struct {
	Id            int
	Flags         int
	StartingLevel int
	Name          string
	Icon          OpenwowIcon
}

type OpenwowCurrency struct {
}

type OpenwowCurrencyCategory struct {
	Id          int
	ExpansionId int
	Flags       int
	Name        string
	Currencies  []struct {
		Id          int
		FactionId   int
		Flags       int
		MaxPerWeek  int
		MaxQuantity int
		Quality     int
		Name        string
		Description string
		Icon        OpenwowIcon
	}
}

type OpenwowRace struct {
	Id            int
	Faction       int
	Flags         int
	StartingLevel int
	Name          string
	NameFemale    string
	FemaleIcon    OpenwowIcon
	MaleIcon      OpenwowIcon
}

type OpenwowSkillLine struct {
	Id          int
	CategoryId  int
	Flags       int
	ParentId    *int
	ParentTier  int
	Description string
	Name        string
	HordeName   string
	NeutralName string
	Icon        OpenwowIcon
}

func StartOpenwowWorker() {
	worker := &OpenwowWorker{}
	go worker.Run()
}

func (ow *OpenwowWorker) Run() {
	ow.GetAllData()
	for _ = range time.Tick(time.Duration(4) * time.Hour) {
		ow.GetAllData()
	}
}

func (ow *OpenwowWorker) GetAllData() {
	ow.GetClasses()
	ow.GetCurrencies()
	ow.GetRaces()
	ow.GetSkillLines()
}

func (ow *OpenwowWorker) GetClasses() {
	log.Info("OpenWoW: fetching classes...")

	data, err := httpGetString(fmt.Sprintf(OPENWOW_DATA_URL, "classes"), 0)
	if err != nil {
		LogError(err, "GetClasses")
		return
	}

	var parsed []OpenwowClass
	if err = json.Unmarshal([]byte(data), &parsed); err != nil {
		LogError(err, "GetClasses")
		return
	}

	var parts []string
	for _, classData := range parsed {
		parts = append(parts, fmt.Sprintf("(%d, '%s', '%s')", classData.Id, classData.Name, classData.Icon.Hash))
	}

	// Doesn't exist, insert
	_, err = db.Exec(fmt.Sprintf(`
INSERT INTO core_characterclass
(id, name, icon)
VALUES
%s
ON CONFLICT (id)
DO UPDATE SET
	name = excluded.name,
	icon = excluded.icon
`, strings.Join(parts, ",")))
	if err != nil {
		LogError(err, "GetClasses")
		return
	}
}

func (ow *OpenwowWorker) GetCurrencies() {
	log.Info("OpenWoW: fetching currencies...")

	data, err := httpGetString(fmt.Sprintf(OPENWOW_DATA_URL, "currency_categories"), 0)
	if err != nil {
		LogError(err, "GetCurrencies")
		return
	}

	var parsed []OpenwowCurrencyCategory
	if err = json.Unmarshal([]byte(data), &parsed); err != nil {
		LogError(err, "GetCurrencies")
		return
	}

	var parts []string
	for _, category := range parsed {
		for _, currency := range category.Currencies {
			parts = append(parts, fmt.Sprintf("(%d, '%s', '%s', %d)", currency.Id, strings.Replace(currency.Name, "'", "''", -1), currency.Icon.Hash, category.Id))
		}
	}

	// Doesn't exist, insert
	_, err = db.Exec(fmt.Sprintf(`
INSERT INTO core_currency
(id, name, icon, category)
VALUES
%s
ON CONFLICT (id)
DO UPDATE SET
	name = excluded.name,
	icon = excluded.icon,
	category = excluded.category
`, strings.Join(parts, ",")))
	if err != nil {
		LogError(err, "GetRaces")
		return
	}
}

func (ow *OpenwowWorker) GetRaces() {
	log.Info("OpenWoW: fetching races...")

	data, err := httpGetString(fmt.Sprintf(OPENWOW_DATA_URL, "races"), 0)
	if err != nil {
		LogError(err, "GetRaces")
		return
	}

	var parsed []OpenwowRace
	if err = json.Unmarshal([]byte(data), &parsed); err != nil {
		LogError(err, "GetRaces")
		return
	}

	var parts []string
	for _, raceData := range parsed {
		parts = append(parts, fmt.Sprintf("(%d, '%s', '%s', '%s', '%s')", raceData.Id, strings.Replace(raceData.Name, "'", "''", -1), FACTIONS[raceData.Faction],
			raceData.FemaleIcon.Hash, raceData.MaleIcon.Hash))
	}

	// Doesn't exist, insert
	_, err = db.Exec(fmt.Sprintf(`
INSERT INTO core_characterrace
(id, name, side, female_icon, male_icon)
VALUES
%s
ON CONFLICT (id)
DO UPDATE SET
	name = excluded.name,
	side = excluded.side,
	female_icon = excluded.female_icon,
	male_icon = excluded.male_icon
`, strings.Join(parts, ",")))
	if err != nil {
		LogError(err, "GetRaces")
		return
	}
}

func (ow *OpenwowWorker) GetSkillLines() {
	log.Info("OpenWoW: fetching skill lines...")

	data, err := httpGetString(fmt.Sprintf(OPENWOW_DATA_URL, "skill_lines"), 0)
	if err != nil {
		LogError(err, "GetSkillLines")
		return
	}

	var parsed []OpenwowSkillLine
	if err = json.Unmarshal([]byte(data), &parsed); err != nil {
		LogError(err, "GetSkillLines")
		return
	}

	var parts []string
	for _, skillLine := range parsed {
		parentId := 0
		if skillLine.ParentId != nil {
			parentId = *skillLine.ParentId
		}

		if skillLine.HordeName == "" {
			skillLine.HordeName = skillLine.Name
		}

		parts = append(parts, fmt.Sprintf("(%d, %d, %d, %d, '%s', '%s', '%s')", skillLine.Id, skillLine.CategoryId, parentId, skillLine.ParentTier,
			strings.Replace(skillLine.Name, "'", "''", -1), strings.Replace(skillLine.HordeName, "'", "''", -1), skillLine.Icon.Hash))
	}

	// Doesn't exist, insert
	_, err = db.Exec(fmt.Sprintf(`
INSERT INTO core_profession
(id, category, parent, tier, alliance_name, horde_name, icon)
VALUES
%s
ON CONFLICT (id)
DO UPDATE SET
	category = excluded.category,
	parent = excluded.parent,
	tier = excluded.tier,
	alliance_name = excluded.alliance_name,
	horde_name = excluded.horde_name,
	icon = excluded.icon
`, strings.Join(parts, ",")))
	if err != nil {
		LogError(err, "GetSkillLines")
		return
	}
}
