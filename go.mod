module gitlabb.com/wowthing/wowthing-backend

require (
	github.com/garyburd/redigo v1.6.0
	github.com/lib/pq v1.0.0
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/scalingdata/gcfg v0.0.0-20140729183856-37aabad69cfd
)
