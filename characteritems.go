package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
)

var contexts = map[string]int{
	"dungeon-normal": 1,
	"dungeon-heroic": 2,
	"raid-normal":    3,
	"raid-heroic":    5,
	"raid-mythic":    6,
	"quest-reward":   11,
	"trade-skill":    13,
	"dungeon-mythic": 23,
	"world-quest-1":  25,
	"world-quest-2":  26,
	"world-quest-3":  27,
	"world-quest-4":  28,
	"world-quest-5":  29,
	"world-quest-6":  30,
}

type CharacterItems struct {
	DeleteIDs []string
	Inserts   []string
	Updates   []string
}

func (ci *CharacterItems) RunQueries() error {
	// Deletes
	if len(ci.DeleteIDs) > 0 {
		_, err := db.Exec(fmt.Sprintf(`
DELETE FROM thing_characteritem
WHERE id IN (%s)
`, strings.Join(ci.DeleteIDs, ", ")))
		if err != nil {
			LogError(err, "RunQueries")
			return err
		}
	}
	ci.DeleteIDs = nil

	// Inserts
	if len(ci.Inserts) > 0 {
		_, err := db.Exec(fmt.Sprintf(`
INSERT INTO thing_characteritem
(character_id, item_id, count, location, extra, container, slot, item_level, quality, context)
VALUES
%s
`, strings.Join(ci.Inserts, ", ")))
		if err != nil {
			LogError(err, "RunQueries")
			return err
		}
	}
	ci.Inserts = nil

	// Updates
	if len(ci.Updates) > 0 {
		_, err := db.Exec(fmt.Sprintf(`
UPDATE thing_characteritem AS ci
SET item_id = new.item_id,
    extra = new.extra,
    item_level = new.item_level,
    quality = new.quality,
    context = new.context
FROM (VALUES
	%s
) AS new(id, item_id, extra, item_level, quality, context)
WHERE ci.id = new.id
`, strings.Join(ci.Updates, ", ")))
		if err != nil {
			LogError(err, "RunQueries")
			return err
		}
	}
	ci.Updates = nil

	return nil
}

func (ci *CharacterItems) UpdateCharacterItem(character *Character, equipped map[int]ItemRow, cItem CharacterItem, slot int) error {
	// Context
	context := contexts[cItem.Context]

	// Azerite Powers
	var powers []int
	for _, power := range cItem.AzeriteEmpoweredItem.AzeritePowers {
		powers = append(powers, power.Id)
	}

	// Gems
	var gems []string
	if cItem.TooltipParams.Gem0 > 0 {
		gems = append(gems, strconv.Itoa(cItem.TooltipParams.Gem0))
	}
	if cItem.TooltipParams.Gem1 > 0 {
		gems = append(gems, strconv.Itoa(cItem.TooltipParams.Gem1))
	}
	if cItem.TooltipParams.Gem2 > 0 {
		gems = append(gems, strconv.Itoa(cItem.TooltipParams.Gem2))
	}

	// Relics
	var relics []string
	for _, relic := range cItem.Relics {
		var parts []string
		parts = append(parts, strconv.Itoa(relic.ItemID))
		for _, bonusID := range relic.BonusLists {
			parts = append(parts, strconv.Itoa(bonusID))
		}
		relics = append(relics, strings.Join(parts, ":"))
	}

	ire := &ItemRowExtra{
		powers,
		cItem.BonusLists,
		cItem.TooltipParams.Enchant,
		strings.Join(gems, ":"),
		relics,
		[]int{cItem.TooltipParams.Upgrade.Current, cItem.TooltipParams.Upgrade.Total, cItem.TooltipParams.Upgrade.ItemLevel},
	}

	b, err := json.Marshal(ire)
	if err != nil {
		LogError(err, "UpdateCharacterItem")
		return err
	}
	extra := string(b)

	// Armory sucks at legendary items
	if cItem.ItemLevel == 895 && cItem.Quality == 5 {
		cItem.ItemLevel = 910
	}

	ir, ok := equipped[slot]
	if ok {
		if cItem.Id == 0 {
			// Item is missing, delete it
			ci.DeleteIDs = append(ci.DeleteIDs, strconv.Itoa(ir.Id))
		} else if ir.ItemId != cItem.Id || ir.Extra != extra || ir.ItemLevel != cItem.ItemLevel || ir.Quality != cItem.Quality {
			// Item has changed, update it
			ci.Updates = append(ci.Updates, fmt.Sprintf("(%d, %d, '%s', %d, %d, %d)", ir.Id, cItem.Id, extra, cItem.ItemLevel, cItem.Quality, context))
		}
	} else {
		// Item is missing, insert it
		if cItem.Id > 0 {
			ci.Inserts = append(ci.Inserts, fmt.Sprintf("(%d, %d, 1, 4, '%s', 0, %d, %d, %d, %d)", character.Id, cItem.Id, extra, slot, cItem.ItemLevel, cItem.Quality, context))
		}
	}

	return nil
}
