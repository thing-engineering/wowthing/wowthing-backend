package main

import (
	"bufio"
	"database/sql"
	"fmt"
	"github.com/garyburd/redigo/redis"
	_ "github.com/lib/pq"
	"github.com/op/go-logging"
	//"github.com/scalingdata/gcfg"
	//"net/http"
	//_ "net/http/pprof"
	"os"
	//"path/filepath"
	"runtime"
	"runtime/debug"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	LOG_FORMAT        = "%{color}%{time:2006-01-02 15:04:05.000000} %{level: -7s}%{color:reset} %{message}"
	USER_AGENT_FORMAT = "WoWthing backend worker (HEAD:%s)"
)

var (
	worker_waitgroup sync.WaitGroup
	db               *sql.DB
	gitRevision      string
	userAgent        string
	accessToken      string

	clientId     = os.Getenv("BNET_KEY")
	clientSecret = os.Getenv("BNET_SECRET")
	log          = logging.MustGetLogger("wtb")
)

// Redis connection pool
var redisPool = &redis.Pool{
	MaxIdle:     2,
	IdleTimeout: 60 * time.Second,
	Dial: func() (redis.Conn, error) {
		c, err := redis.Dial("tcp", Config.Redis.ConnectionString)
		if err != nil {
			return nil, err
		}
		c.Do("SELECT", Config.Redis.Database)
		return c, err
	},
	TestOnBorrow: func(c redis.Conn, t time.Time) error {
		_, err := c.Do("PING")
		return err
	},
}

var Config struct {
	General struct {
		Database         string
		AuctionLimited   bool
		CacheInterval    int
		AuctionWorkers   int
		CharacterWorkers int
		ItemWorkers      int
		SpellWorkers     int
	}
	Redis struct {
		ConnectionString string
		Database         int
	}
}

func main() {
	// Set up logging
	logBackend := logging.NewLogBackend(os.Stderr, "", 0)
	logging.SetBackend(logBackend)
	logging.SetFormatter(logging.MustStringFormatter(LOG_FORMAT))
	logging.SetLevel(logging.DEBUG, "wtb")
	// logging.SetLevel(logging.INFO, "wtb")

	setUserAgent()

	log.Infof("wowthing-backend HEAD:%s starting", gitRevision)

	log.Debugf("GOMAXPROCS: %d", runtime.GOMAXPROCS(0))

	// Load config file
	/*var cfgFile = filepath.Join(".", "wtb.conf")

	log.Debugf("Reading config from %s", cfgFile)
	err := gcfg.ReadFileInto(&Config, cfgFile)
	if err != nil {
		log.Fatal(err)
	}*/

	// Load config from environment
	Config.General.Database = fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", os.Getenv("POSTGRES_USER"), os.Getenv("POSTGRES_PASS"), os.Getenv("POSTGRES_HOST"), os.Getenv("POSTGRES_PORT"), os.Getenv("POSTGRES_DATABASE"))
	Config.Redis.ConnectionString = fmt.Sprintf("%s:%s", os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PORT"))
	database, err := strconv.Atoi(os.Getenv("REDIS_DATABASE"))
	if err != nil {
		LogError(err, "main")
		database = 0
	}
	Config.Redis.Database = database

	// Defaults because ugh
	Config.General.AuctionLimited = true
	Config.General.CacheInterval = 1
	Config.General.AuctionWorkers = 2
	Config.General.CharacterWorkers = 8
	Config.General.ItemWorkers = 4
	Config.General.SpellWorkers = 4

	// Open database
	db, err = sql.Open("postgres", Config.General.Database)
	if err != nil {
		log.Fatal(err)
	}
	db.SetMaxOpenConns(Config.General.AuctionWorkers + Config.General.CharacterWorkers + Config.General.ItemWorkers)
	db.SetMaxIdleConns(1)

	// Auction channel
	achan := make(chan *AuctionRealm, 1000)
	// Character channel
	cchan := make(chan *Character, 10000)
	// Item channel
	ichan := make(chan int, 10000)
	// Spell channel
	schan := make(chan int, 10000)

	// Start rate limited API workers
	StartAuctionWorkers(time.Tick(100*time.Millisecond), achan)
	StartCharacterWorkers(time.Tick(20*time.Millisecond), cchan)
	StartItemWorkers(time.Tick(100*time.Millisecond), ichan)
	StartSpellWorkers(time.Tick(100*time.Millisecond), schan)

	StartOpenwowWorker()

	go AccessTokenThing()

	// Cacher thingo
	go Cacher()

	// History workers
	go AuctionHistoryWorker()
	go HistoryWorker()

	go periodicFree(1 * time.Minute)
	go LoadTransmogJson()

	// Run the thing
	Scheduler(achan, cchan, ichan, schan)

	// Close the job channels and wait for workers to finish
	close(achan)
	close(cchan)
	close(ichan)
	close(schan)
	worker_waitgroup.Wait()
}

func setUserAgent() {
	gitRevision, err := getGitRevision()
	if err != nil {
		log.Error(err)
		gitRevision = "unknown"
	}
	userAgent = fmt.Sprintf(USER_AGENT_FORMAT, gitRevision)
}

func getGitRevision() (string, error) {
	// Get our revision
	f, err := os.Open(".git/refs/heads/master")
	if err != nil {
		return "", err
	}
	defer f.Close()

	r := bufio.NewReader(f)
	gitRevision, err = r.ReadString('\n')
	if err != nil {
		return "", err
	}

	gitRevision = strings.TrimSuffix(gitRevision, "\n")
	return gitRevision, nil
}

func periodicFree(d time.Duration) {
	tick := time.Tick(d)
	for _ = range tick {
		debug.FreeOSMemory()
	}
}
