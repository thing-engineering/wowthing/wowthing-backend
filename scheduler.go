package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

const (
	AUCTION_READY_QUERY = `
SELECT  id, region, slug
FROM    core_realm
WHERE   first_realm = true
`

	AUCTION_ACTIVE_READY_QUERY = `
SELECT  id, region, slug
FROM    core_realm
WHERE   first_realm = true AND
        long_name IN (
            SELECT  long_name
            FROM    core_realm
            WHERE id IN (
                SELECT  DISTINCT realm_id
                FROM    thing_character
            )
        )
`

	CHARACTER_READY_QUERY = `
SELECT c.id,
       c.name,
       c.level,
       r.region,
       r.slug,
       COALESCE(c.bnetaccount_id, 0),
       c.last_modified
FROM thing_character c
INNER JOIN core_realm r ON c.realm_id = r.id
LEFT OUTER JOIN core_bnetaccount b ON c.bnetaccount_id = b.id
LEFT OUTER JOIN core_userprofile p ON b.user_id = p.user_id
WHERE (
          c.last_api_update IS NULL
          OR current_timestamp - c.last_api_update > (
              '10 minutes'::interval +
              ('1 minute'::interval * LEAST(72, GREATEST(0, 120 - c.level))) +
              ('1 hour'::interval * LEAST(24, EXTRACT(EPOCH FROM current_timestamp - COALESCE(p.last_visit, current_timestamp - '24 hours'::interval)) / 86400)) +
              ('8 hours'::interval * LEAST(21, c.count_404))
          )
      )
      AND c.level >= 10
ORDER BY c.last_api_update NULLS FIRST
LIMIT 1000
`
)

type AuctionRealm struct {
	Id     int
	Region string
	Slug   string
}

type Character struct {
	Id            int
	Name          string
	Level         int
	Region        string
	Realm         string
	BnetAccountId int
	LastModified  int
}

func Scheduler(achan chan *AuctionRealm, cchan chan *Character, ichan chan int, schan chan int) {
	log.Info("Scheduler started")

	counter := 0

	for _ = range time.Tick(10 * time.Second) {
		if accessToken == "" {
			log.Warning("YIKES")
			continue
		}

		if counter == 0 {
			for _, auctionRealm := range fetchAuctionRealms() {
				achan <- auctionRealm
			}
		}

		if len(cchan) < 100 {
			for _, character := range fetchCharacters() {
				cchan <- character
			}
		}

		if len(ichan) < 100 {
			for _, itemId := range fetchItems() {
				ichan <- itemId
			}
		}

		if len(schan) < 100 {
			for _, spellId := range fetchSpells() {
				schan <- spellId
			}
		}

		counter = (counter + 1) % 6
	}
}

func fetchAuctionRealms() []*AuctionRealm {
	var query string
	if Config.General.AuctionLimited {
		query = AUCTION_ACTIVE_READY_QUERY
	} else {
		query = AUCTION_READY_QUERY
	}

	var auctionRealms []*AuctionRealm

	rows, err := db.Query(query)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		auctionRealm := &AuctionRealm{}
		err := rows.Scan(&auctionRealm.Id, &auctionRealm.Region, &auctionRealm.Slug)
		if err != nil {
			log.Fatal(err)
		}
		auctionRealms = append(auctionRealms, auctionRealm)
	}

	return auctionRealms
}

func fetchCharacters() []*Character {
	rows, err := db.Query(CHARACTER_READY_QUERY)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var characters []*Character
	var updateIds []string

	for rows.Next() {
		character := &Character{}
		err := rows.Scan(&character.Id, &character.Name, &character.Level, &character.Region, &character.Realm, &character.BnetAccountId, &character.LastModified)
		if err != nil {
			log.Fatal(err)
		}
		characters = append(characters, character)
		updateIds = append(updateIds, strconv.Itoa(character.Id))
	}

	if len(updateIds) > 0 {
		_, err = db.Exec(fmt.Sprintf(`
UPDATE thing_character
SET last_api_update = now()
WHERE id IN (%s)
`, strings.Join(updateIds, ", ")))
		if err != nil {
			log.Fatal(err)
		}
	}

	return characters
}

func fetchItems() []int {
	rows, err := db.Query(`
SELECT id
FROM core_item
WHERE name = '###UPDATE###'
ORDER BY id
LIMIT 1000
`)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var items []int
	var updateIds []string

	var itemId int
	for rows.Next() {
		err := rows.Scan(&itemId)
		if err != nil {
			log.Fatal(err)
		}
		items = append(items, itemId)
		updateIds = append(updateIds, strconv.Itoa(itemId))
	}

	if len(updateIds) > 0 {
		_, err = db.Exec(fmt.Sprintf(`
UPDATE core_item
SET name = '###UPDATING###'
WHERE id IN (%s)
`, strings.Join(updateIds, ", ")))
		if err != nil {
			log.Fatal(err)
		}
	}

	return items
}

func fetchSpells() []int {
	rows, err := db.Query(`
SELECT id
FROM core_spell
WHERE name = '###UPDATE###'
ORDER BY id
LIMIT 1000
`)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var spells []int
	var updateIds []string

	var spellId int
	for rows.Next() {
		err := rows.Scan(&spellId)
		if err != nil {
			log.Fatal(err)
		}
		spells = append(spells, spellId)
		updateIds = append(updateIds, strconv.Itoa(spellId))
	}

	if len(updateIds) > 0 {
		_, err = db.Exec(fmt.Sprintf(`
UPDATE core_spell
SET name = '###UPDATING###'
WHERE id IN (%s)
`, strings.Join(updateIds, ", ")))
		if err != nil {
			log.Fatal(err)
		}
	}

	return spells
}
