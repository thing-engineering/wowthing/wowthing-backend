package main

import (
	"crypto/md5"
	//"crypto/sha1"
	"encoding/json"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"reflect"
	"strconv"
	"strings"
	"time"
)

const (
	API_CHARACTER        = "https://%s.api.blizzard.com/wow/character/%s/%s?fields=achievements,items,mounts,pets,professions,pvp,quests,reputation"
	API_EXTRA            = "%s&locale=en_US&access_token=%s"
	API_MYTHIC_KEYSTONE  = "https://%s.api.blizzard.com/profile/wow/character/%s/%s/mythic-keystone-profile?namespace=profile-%s"
	CHARACTER_CACHE_TIME = 240
)

var itemSlots = map[string]int{
	"Head":     1,
	"Neck":     2,
	"Shoulder": 3,
	"Back":     15,
	"Chest":    5,
	"Shirt":    4,
	"Tabard":   19,
	"Wrist":    9,
	"Hands":    10,
	"Waist":    6,
	"Legs":     7,
	"Feet":     8,
	"Finger1":  11,
	"Finger2":  12,
	"Trinket1": 13,
	"Trinket2": 14,
	"MainHand": 16,
	"OffHand":  17,
}

// Reputations to skip due to API shittiness
var skipReputations = map[int]bool{
	67:  true, // Horde
	469: true, // Alliance
}

type ArrayData struct {
	Achievements          string
	AchievementsTimestamp string
	Criteria              string
	CriteriaQuantity      string
	Mounts                string
	Quests                string
}
type PetData struct {
	Id int

	Favourite bool   `json:"isFavorite"`
	Guid      string `json:"battlePetGuid"`
	Name      string
	PetId     int `json:"creatureId"`
	Quality   int `json:"qualityId"`

	Stats struct {
		Level int
	}
}
type ProfessionData struct {
	Id      int
	Rank    int
	Max     int
	Recipes []int
}
type ProfessionRow struct {
	Id         int
	Rank       int
	Max        int
	RecipeHash string
}
type ReputationData struct {
	Id       int
	Standing int
	Value    int
	Max      int
}
type CharacterItem struct {
	Id         int
	Context    string
	ItemLevel  int
	Quality    int
	BonusLists []int

	AzeriteEmpoweredItem struct {
		AzeritePowers []struct {
			Id int
		}
	}

	AzeriteItem struct {
		AzeriteLevel               int
		AzeriteExperience          int
		AzeriteExperienceRemaining int
	}

	Relics []struct {
		ItemID     int
		BonusLists []int
	}

	TooltipParams struct {
		Enchant int
		Gem0    int
		Gem1    int
		Gem2    int

		Upgrade struct {
			Current   int
			Total     int
			ItemLevel int `json:"itemLevelIncrement"`
		}
	}
}
type CharacterData struct {
	Class               int
	Race                int
	Gender              int
	Level               int
	AchievementPoints   int
	TotalHonorableKills int
	LastModified        int64

	Achievements struct {
		Achievements      []int `json:"achievementsCompleted"`
		AchievementStamps []int `json:"achievementsCompletedTimestamp"`
		Criteria          []int
		CriteriaQuantity  []int
	}

	Items struct {
		Head     CharacterItem
		Neck     CharacterItem
		Shoulder CharacterItem
		Back     CharacterItem
		Chest    CharacterItem
		Shirt    CharacterItem
		Tabard   CharacterItem
		Wrist    CharacterItem
		Hands    CharacterItem
		Waist    CharacterItem
		Legs     CharacterItem
		Feet     CharacterItem
		Finger1  CharacterItem
		Finger2  CharacterItem
		Trinket1 CharacterItem
		Trinket2 CharacterItem
		MainHand CharacterItem
		OffHand  CharacterItem
	}

	Mounts struct {
		Collected []struct {
			SpellId int
		}
	}

	Pets struct {
		Collected []*PetData
	}

	Professions struct {
		Primary   []*ProfessionData
		Secondary []*ProfessionData
	}

	Quests []int

	Reputation []*ReputationData
}

type ItemRow struct {
	Id        int
	ItemId    int
	ItemLevel int
	Quality   int
	Context   int
	Extra     string
}

type ItemRowExtra struct {
	AzeritePowers []int    `json:"a"`
	Bonuses       []int    `json:"b"`
	Enchant       int      `json:"e"`
	Gems          string   `json:"g"`
	Relics        []string `json:"r"`
	Upgrade       []int    `json:"u"`
}

// Mythic Keystone
type MythicKeystoneData struct {
	CurrentPeriod struct {
		Period struct {
			Id int
		}

		BestRuns []MythicBestRun `json:"best_runs"`
	} `json:"current_period"`

	Seasons []struct {
		Key struct {
			Href string
		}
		Id int
	}
}

type MythicSeasonData struct {
	Season struct {
		Id int
	}
	BestRuns []MythicBestRun `json:"best_runs"`
}

type MythicBestRun struct {
	Completed int64 `json:"completed_timestamp"`
	Duration  int
	Level     int  `json:"keystone_level"`
	Timed     bool `json:"is_completed_within_time"`

	Affixes []struct {
		Id int
	} `json:"keystone_affixes"`
	Dungeon struct {
		Id int
	}
}

type CharacterWorker struct {
	throttle <-chan time.Time
	id       int
	cchan    chan *Character
}

func StartCharacterWorkers(throttle <-chan time.Time, cchan chan *Character) {
	for i := 1; i <= Config.General.CharacterWorkers; i++ {
		worker := &CharacterWorker{
			throttle,
			i,
			cchan,
		}
		go worker.run()
	}
}

func (cw *CharacterWorker) run() {
	// Add this worker to the wait group
	worker_waitgroup.Add(1)
	defer worker_waitgroup.Done()

	log.Infof("CharacterWorker %d started", cw.id)

	for character := range cw.cchan {
		t1 := time.Now()

		// Character data
		data, err := cw.fetchCharacterAPI(character)
		if err != nil {
			httpErr, ok := err.(HttpError)
			if ok && httpErr.StatusCode == 304 {
				log.Debugf("Char%d: %s/%s/%s not modified - API: %s", cw.id, character.Region, character.Realm, character.Name, time.Since(t1))
				continue
			}

			cw.LogError(err, character)

			// Bump this character's 404 counter
			if ok && httpErr.StatusCode == 404 {
				_, err := db.Exec(`
UPDATE thing_character
SET count_404 = count_404 + 1
WHERE id = $1
`, character.Id)
				if err != nil {
					cw.LogError(err, character)
				}
			}

			continue
		}

		t2 := time.Now()

		var cData CharacterData
		if err = json.Unmarshal([]byte(data), &cData); err != nil {
			cw.LogError(err, character)
			continue
		}

		if err = cw.updateCharacter(character, &cData); err != nil {
			continue
		}

		t3 := time.Now()

		// Mythic keystone data
		if character.Level == 120 {
			data, err = cw.fetchKeystoneAPI(character)
			if err != nil {
				cw.LogError(err, character)
				continue
			}

			var mData MythicKeystoneData
			if err = json.Unmarshal([]byte(data), &mData); err != nil {
				cw.LogError(err, character)
				continue
			}

			if err = cw.updateKeystone(character, &mData); err != nil {
				continue
			}
		}

		t4 := time.Now()

		log.Debugf("Char%d: %s/%s/%s character - API: %s, work: %s, keystone: %s", cw.id, character.Region, character.Realm, character.Name, t2.Sub(t1), t3.Sub(t2), t4.Sub(t3))
	}
}

func (cw *CharacterWorker) LogError(err error, character *Character) {
	LogErrorN(err, 2, "Char%d: %s/%s/%s", cw.id, character.Region, character.Realm, character.Name)
}

func (cw *CharacterWorker) fetchCharacterAPI(character *Character) (string, error) {
	// Build the URL
	url := fmt.Sprintf(API_CHARACTER, character.Region, character.Realm, character.Name)

	// Don't overrun our per-second limit
	<-cw.throttle

	// Go fetch
	data, err := httpGetString(fmt.Sprintf(API_EXTRA, url, accessToken), character.LastModified)
	if err != nil {
		return "", err
	}

	return data, nil
}

func (cw *CharacterWorker) fetchKeystoneAPI(character *Character) (string, error) {
	// Build the URL
	url := fmt.Sprintf(API_MYTHIC_KEYSTONE, character.Region, character.Realm, strings.ToLower(character.Name), character.Region)

	return cw.fetchGenericAPI(url)
}

func (cw *CharacterWorker) fetchGenericAPI(url string) (string, error) {
	// Don't overrun our per-second limit
	<-cw.throttle

	// Go fetch
	data, err := httpGetString(fmt.Sprintf(API_EXTRA, url, accessToken), 0)
	if err != nil {
		return "", err
	}

	return data, nil
}

func (cw *CharacterWorker) updateCharacter(character *Character, cData *CharacterData) error {
	// Update basic data
	if cData.Race > 0 {
		_, err := db.Exec(`
UPDATE thing_character
SET cls_id = $2,
    race_id = $3,
    gender = $4,
    level = $5,
    honorable_kills = $6,
    last_modified = $7,
    azerite_level = $8,
    azerite_current_xp = $9,
    azerite_level_xp = $10,
    count_404 = 0
WHERE id = $1
`, character.Id, cData.Class, cData.Race, cData.Gender, cData.Level, cData.TotalHonorableKills, cData.LastModified/1000,
			cData.Items.Neck.AzeriteItem.AzeriteLevel, cData.Items.Neck.AzeriteItem.AzeriteExperience, cData.Items.Neck.AzeriteItem.AzeriteExperienceRemaining)
		if err != nil {
			cw.LogError(err, character)
			return err
		}
	}

	// Update guild data?

	// Update equipped items
	if err := cw.updateItems(character, cData); err != nil {
		return err
	}

	// Update arrays
	if err := cw.updateArrays(character, cData); err != nil {
		return err
	}

	// Update pets if we should be
	if character.BnetAccountId > 0 && len(cData.Pets.Collected) > 0 {
		if err := cw.updatePets(character, cData); err != nil {
			// Pets suck
			//return err
		}
	}

	// Update profession data
	if err := cw.updateProfessions(character, cData); err != nil {
		return err
	}

	// Update reputation data
	if err := cw.updateReputations(character, cData); err != nil {
		return err
	}

	return nil
}

func (cw *CharacterWorker) updateKeystone(character *Character, mData *MythicKeystoneData) error {
	// Update max completed keystone
	maxKeystone := 0
	for _, run := range mData.CurrentPeriod.BestRuns {
		if run.Level > maxKeystone {
			maxKeystone = run.Level
		}
	}

	// Update basic data
	_, err := db.Exec(`
UPDATE thing_character
SET keystone_max = $2,
    keystone_period = $3
WHERE id = $1
`, character.Id, maxKeystone, mData.CurrentPeriod.Period.Id)
	if err != nil {
		cw.LogError(err, character)
		return err
	}

	// Update seasons
	row := db.QueryRow(`
SELECT COALESCE(MAX(season), 0)
FROM thing_charactermythicplus
WHERE character_id = $1
`, character.Id)
	var maxDbSeason int
	err = row.Scan(&maxDbSeason)
	if err != nil {
		cw.LogError(err, character)
		return err
	}

	if mData.Seasons != nil && len(mData.Seasons) > 0 {
		checkSkip := (maxDbSeason == mData.Seasons[len(mData.Seasons)-1].Id)
		for _, season := range mData.Seasons {
			if checkSkip && season.Id < maxDbSeason {
				continue
			}
			cw.updateKeystoneSeason(character, season.Id, season.Key.Href)
		}
	}

	return nil
}

func (cw *CharacterWorker) updateKeystoneSeason(character *Character, season int, url string) error {
	data, err := cw.fetchGenericAPI(url)
	if err != nil {
		cw.LogError(err, character)
		return err
	}

	var sData MythicSeasonData
	if err = json.Unmarshal([]byte(data), &sData); err != nil {
		cw.LogError(err, character)
		return err
	}

	var parts []string
	for _, run := range sData.BestRuns {
		affixIDs := make([]string, len(run.Affixes))
		for i, affix := range run.Affixes {
			affixIDs[i] = strconv.Itoa(affix.Id)
		}
		affixArray := fmt.Sprintf("{%s}", strings.Join(affixIDs, ","))

		parts = append(parts, fmt.Sprintf("(%d, %d, %d, %d, %d, %d, %t, '%s')", character.Id, season, run.Dungeon.Id, run.Level, run.Completed/1000, run.Duration, run.Timed, affixArray))
	}

	// Doesn't exist, insert
	_, err = db.Exec(fmt.Sprintf(`
INSERT INTO thing_charactermythicplus
(character_id, season, dungeon_id, level, completed_at, duration, in_time, affixes)
VALUES
%s
ON CONFLICT (character_id, season, dungeon_id, in_time)
DO UPDATE SET
	level = excluded.level,
	completed_at = excluded.completed_at,
	duration = excluded.duration,
	affixes = excluded.affixes
`, strings.Join(parts, ",")))
	if err != nil {
		cw.LogError(err, character)
		return err
	}

	return nil
}

// Update equipped items
func (cw *CharacterWorker) updateItems(character *Character, cData *CharacterData) error {
	// Build a list of itemIDs
	itemIDs := cw.buildItemIDs([]CharacterItem{
		cData.Items.Head,
		cData.Items.Neck,
		cData.Items.Shoulder,
		cData.Items.Back,
		cData.Items.Chest,
		cData.Items.Shirt,
		cData.Items.Tabard,
		cData.Items.Wrist,
		cData.Items.Hands,
		cData.Items.Waist,
		cData.Items.Legs,
		cData.Items.Feet,
		cData.Items.Finger1,
		cData.Items.Finger2,
		cData.Items.Trinket1,
		cData.Items.Trinket2,
		cData.Items.MainHand,
		cData.Items.OffHand,
	})
	if len(itemIDs) == 0 {
		return nil
	}

	// See if any of these IDs don't exist
	var tempParts []string
	for _, itemID := range itemIDs {
		tempParts = append(tempParts, fmt.Sprintf("(%s)", itemID))
	}
	temp := strings.Join(tempParts, ",")

	rows, err := db.Query(fmt.Sprintf(`
SELECT id
FROM (VALUES %s) AS temp(id)
WHERE id NOT IN (
	SELECT id
	FROM core_item
	WHERE id IN (%s)
)
`, temp, strings.Join(itemIDs, ",")))
	if err != nil {
		cw.LogError(err, character)
		return err
	}
	defer rows.Close()

	//var parts []string
	var checkItems []int
	for rows.Next() {
		var id int
		if err = rows.Scan(&id); err != nil {
			cw.LogError(err, character)
			return err
		}
		checkItems = append(checkItems, id)
		//parts = append(parts, fmt.Sprintf("(%d, '###UPDATE###', '', '', 0, 0, 0, '{}'::integer[], 0, 0, 0, 0, '{}'::integer[], 0)", id))
	}

	err = InsertItems(checkItems)
	if err != nil {
		cw.LogError(err, character)
		return err
	}

	// Grab character equipped items
	rows, err = db.Query(`
SELECT id,
       item_id,
       item_level,
       quality,
       extra,
       slot,
       context
FROM thing_characteritem
WHERE character_id = $1
      AND location = 4
`, character.Id)
	if err != nil {
		cw.LogError(err, character)
		return err
	}
	defer rows.Close()

	equipped := make(map[int]ItemRow)
	for rows.Next() {
		var itemRow ItemRow
		var slot int
		if err := rows.Scan(&itemRow.Id, &itemRow.ItemId, &itemRow.ItemLevel, &itemRow.Quality, &itemRow.Extra, &slot, &itemRow.Context); err != nil {
			cw.LogError(err, character)
			return err
		}

		// Unmarshal the damn slot info
		/*var extra ItemRowExtra
		if err = json.Unmarshal([]byte(itemRow.Extra), &extra); err != nil {
			cw.LogError(err, character)
			return err
		}*/
		equipped[slot] = itemRow
	}

	ci := &CharacterItems{}

	val := reflect.ValueOf(&cData.Items).Elem()
	for i := 0; i < val.NumField(); i++ {
		typeField := val.Type().Field(i)

		valueField := val.Field(i).Interface().(CharacterItem)

		ci.UpdateCharacterItem(character, equipped, valueField, itemSlots[typeField.Name])
	}

	err = ci.RunQueries()
	if err != nil {
		cw.LogError(err, character)
		return err
	}

	return nil
}

func (cw *CharacterWorker) buildItemIDs(cItems []CharacterItem) []string {
	var itemIDs []string
	for i := range cItems {
		if cItems[i].Id > 0 {
			itemIDs = append(itemIDs, strconv.Itoa(cItems[i].Id))
		}
	}
	return itemIDs
}

// Update arrays (achievements, mounts)
func (cw *CharacterWorker) updateArrays(character *Character, cData *CharacterData) error {
	// Get a Redis connection
	conn := redisPool.Get()
	defer conn.Close()

	// Pull the old data from the database
	oldData := ArrayData{}
	err := db.QueryRow(`
SELECT MD5(COALESCE(achievements::text, '')),
       MD5(COALESCE(achievements_timestamp::text, '')),
       MD5(COALESCE(criteria::text, '')),
       MD5(COALESCE(criteria_quantity::text, '')),
       MD5(COALESCE(mounts::text, '')),
       quests_md5
FROM thing_characterdata
WHERE character_id = $1
`, character.Id).Scan(&oldData.Achievements, &oldData.AchievementsTimestamp, &oldData.Criteria, &oldData.CriteriaQuantity, &oldData.Mounts, &oldData.Quests)
	if err != nil {
		cw.LogError(err, character)
		return err
	}

	newData := ArrayData{}

	var achievements []string
	for _, a := range cData.Achievements.Achievements {
		achievements = append(achievements, strconv.Itoa(a))
	}
	achieveArray := fmt.Sprintf("{%s}", strings.Join(achievements, ","))
	newData.Achievements = fmt.Sprintf("%x", md5.Sum([]byte(achieveArray)))

	var achievementStamps []string
	for _, s := range cData.Achievements.AchievementStamps {
		achievementStamps = append(achievementStamps, strconv.Itoa(s/1000))
	}
	stampArray := fmt.Sprintf("{%s}", strings.Join(achievementStamps, ","))
	newData.AchievementsTimestamp = fmt.Sprintf("%x", md5.Sum([]byte(stampArray)))

	var criteria []string
	for _, c := range cData.Achievements.Criteria {
		criteria = append(criteria, strconv.Itoa(c))
	}
	critArray := fmt.Sprintf("{%s}", strings.Join(criteria, ","))
	newData.Criteria = fmt.Sprintf("%x", md5.Sum([]byte(critArray)))

	var criteriaQuantity []string
	for _, q := range cData.Achievements.CriteriaQuantity {
		criteriaQuantity = append(criteriaQuantity, strconv.Itoa(q))
	}
	qtyArray := fmt.Sprintf("{%s}", strings.Join(criteriaQuantity, ","))
	newData.CriteriaQuantity = fmt.Sprintf("%x", md5.Sum([]byte(qtyArray)))

	var spellIDs []string
	for _, mount := range cData.Mounts.Collected {
		spellIDs = append(spellIDs, strconv.Itoa(mount.SpellId))
	}
	mountArray := fmt.Sprintf("{%s}", strings.Join(spellIDs, ","))
	newData.Mounts = fmt.Sprintf("%x", md5.Sum([]byte(mountArray)))

	var questIDs []string
	for _, questID := range cData.Quests {
		questIDs = append(questIDs, strconv.Itoa(questID))
	}
	questArray := fmt.Sprintf("{%s}", strings.Join(questIDs, ","))
	newData.Quests = fmt.Sprintf("%x", md5.Sum([]byte(questArray)))

	var parts []string
	redisHashParts := make(map[string]bool)
	if newData.Achievements != oldData.Achievements {
		parts = append(parts, fmt.Sprintf("achievements = '%s'", achieveArray))
		redisHashParts["achievements"] = true
	}
	if newData.AchievementsTimestamp != oldData.AchievementsTimestamp {
		parts = append(parts, fmt.Sprintf("achievements_timestamp = '%s'", stampArray))
		redisHashParts["achievements"] = true
	}
	if newData.Criteria != oldData.Criteria {
		parts = append(parts, fmt.Sprintf("criteria = '%s'", critArray))
		redisHashParts["criteria"] = true
	}
	if newData.CriteriaQuantity != oldData.CriteriaQuantity {
		parts = append(parts, fmt.Sprintf("criteria_quantity = '%s'", qtyArray))
		redisHashParts["criteria"] = true
	}
	if newData.Mounts != oldData.Mounts {
		parts = append(parts, fmt.Sprintf("mounts = '%s'", mountArray))
		redisHashParts["mounts"] = true
	}
	if newData.Quests != oldData.Quests {
		parts = append(parts, fmt.Sprintf("quests = '%s'", questArray))
		parts = append(parts, fmt.Sprintf("quests_md5 = '%s'", newData.Quests))
		redisHashParts["quests"] = true
	}

	// Update any data that changed
	if len(parts) > 0 {
		_, err := db.Exec(fmt.Sprintf(`
UPDATE thing_characterdata
SET %s
WHERE character_id = %d
`, strings.Join(parts, ", "), character.Id))
		if err != nil {
			cw.LogError(err, character)
			return err
		}
	}

	// Set cache times in Redis for anything that changed
	if character.BnetAccountId > 0 && len(redisHashParts) > 0 {
		key := fmt.Sprintf("updated:%d", character.BnetAccountId)
		now := fmt.Sprintf("%v", time.Now().UTC().Unix())

		redisParams := []interface{}{key}
		for k := range redisHashParts {
			redisParams = append(redisParams, k, now)
		}

		_, err := redis.String(conn.Do("HMSET", redisParams...))
		if err != nil {
			cw.LogError(err, character)
		}
	}

	return nil
}

// Update pets
func (cw *CharacterWorker) updatePets(character *Character, cData *CharacterData) error {
	// Get a Redis connection
	conn := redisPool.Get()
	defer conn.Close()

	// Redis lock to only update this account's pets once per cycle
	//if character.BnetAccountId > 0 {
	lockKey := fmt.Sprintf("petlock:%d", character.BnetAccountId)
	_, err := redis.String(conn.Do("SET", lockKey, "1", "EX", CHARACTER_CACHE_TIME, "NX"))
	if err != nil {
		return nil
	}
	//}

	rows, err := db.Query(`
SELECT id,
       pet_id,
       favourite,
       guid,
       level,
       name,
       quality
FROM thing_accountpet
WHERE bnetaccount_id = $1
`, character.BnetAccountId)
	if err != nil {
		cw.LogError(err, character)
		return err
	}
	defer rows.Close()

	// Build a GUID:PetData map
	petMap := make(map[string]*PetData)
	var seenGuids []string

	for rows.Next() {
		pet := &PetData{}
		err := rows.Scan(&pet.Id, &pet.PetId, &pet.Favourite, &pet.Guid, &pet.Stats.Level, &pet.Name, &pet.Quality)
		if err != nil {
			cw.LogError(err, character)
			return err
		}

		if _, ok := petMap[pet.Guid]; ok {
			log.Errorf("Char%d: duplicate pet GUID %s", cw.id, pet.Guid)
		}

		petMap[pet.Guid] = pet
	}

	// Iterate over pets
	for _, newPet := range cData.Pets.Collected {
		seenGuids = append(seenGuids, fmt.Sprintf("'%s'", newPet.Guid))

		oldPet, ok := petMap[newPet.Guid]
		if ok {
			// Exists and changed, update row
			if newPet.PetId != oldPet.PetId || newPet.Favourite != oldPet.Favourite || newPet.Stats.Level != oldPet.Stats.Level || newPet.Name != oldPet.Name || newPet.Quality != oldPet.Quality {
				_, err := db.Exec(`
UPDATE thing_accountpet
SET pet_id = $2,
	favourite = $3,
	level = $4,
	name = $5,
	quality = $6
WHERE id = $1
`, oldPet.Id, newPet.PetId, newPet.Favourite, newPet.Stats.Level, newPet.Name, newPet.Quality)
				if err != nil {
					cw.LogError(err, character)
					return err
				}
			}
		} else {
			// Doesn't exist, insert
			_, err := db.Exec(`
INSERT INTO thing_accountpet
(bnetaccount_id, pet_id, favourite, guid, level, name, quality)
VALUES
($1, $2, $3, $4, $5, $6, $7)
`, character.BnetAccountId, newPet.PetId, newPet.Favourite, newPet.Guid, newPet.Stats.Level, newPet.Name, newPet.Quality)
			if err != nil {
				cw.LogError(err, character)
				return err
			}
		}
	}

	// Delete any leftovers
	if len(seenGuids) > 0 {
		_, err = db.Exec(fmt.Sprintf(`
DELETE FROM thing_accountpet
WHERE bnetaccount_id = $1 AND
      pet_id NOT IN (33239, 73809) AND
      guid NOT IN (%s)
	`, strings.Join(seenGuids, ", ")), character.BnetAccountId)
		if err != nil {
			cw.LogError(err, character)
			return err
		}
	}

	// Set cache time in Redis
	if character.BnetAccountId > 0 {
		key := fmt.Sprintf("updated:%d", character.BnetAccountId)
		now := fmt.Sprintf("%v", time.Now().UTC().Unix())

		if _, err := conn.Do("HSET", key, "pets", now); err != nil {
			cw.LogError(err, character)
			return err
		}
	}

	return nil
}

// Update professions
func (cw *CharacterWorker) updateProfessions(character *Character, cData *CharacterData) error {
	rows, err := db.Query(`
SELECT profession_id,
       current_skill,
       max_skill,
       MD5(recipes::text)
FROM thing_characterprofession
WHERE character_id = $1
ORDER BY skill_type,
         id
`, character.Id)
	if err != nil {
		cw.LogError(err, character)
		return err
	}
	defer rows.Close()

	profMap := make(map[int]ProfessionRow)

	for rows.Next() {
		//var id, profId int
		prof := ProfessionRow{}
		err := rows.Scan(&prof.Id, &prof.Rank, &prof.Max, &prof.RecipeHash)
		if err != nil {
			cw.LogError(err, character)
			return err
		}

		profMap[prof.Id] = prof
	}

	// Iterate over primary and secondary professions
	var seenIds []string

	for _, prof := range cData.Professions.Primary {
		seenIds = append(seenIds, strconv.Itoa(prof.Id))
		if err = cw.doCharacterProfession(profMap, character, prof, 1); err != nil {
			cw.LogError(err, character)
			return err
		}
	}
	for _, prof := range cData.Professions.Secondary {
		seenIds = append(seenIds, strconv.Itoa(prof.Id))
		if err = cw.doCharacterProfession(profMap, character, prof, 2); err != nil {
			cw.LogError(err, character)
			return err
		}
	}

	// Delete any leftovers
	if len(seenIds) > 0 {
		_, err = db.Exec(fmt.Sprintf(`
	DELETE FROM thing_characterprofession
	WHERE character_id = $1 AND
	      profession_id NOT IN (%s)
	`, strings.Join(seenIds, ", ")), character.Id)
		if err != nil {
			cw.LogError(err, character)
			return err
		}
	}

	return nil
}

func (cw *CharacterWorker) doCharacterProfession(profMap map[int]ProfessionRow, character *Character, prof *ProfessionData, skillType int) error {
	// Build recipes array string
	var recipeStrings []string
	for _, recipeId := range prof.Recipes {
		recipeStrings = append(recipeStrings, strconv.Itoa(recipeId))
	}
	recipes := fmt.Sprintf("{%s}", strings.Join(recipeStrings, ","))
	recipeHash := fmt.Sprintf("%x", md5.Sum([]byte(recipes)))

	oldProf, ok := profMap[prof.Id]
	if !ok {
		// Doesn't exist, insert
		_, err := db.Exec(fmt.Sprintf(`
INSERT INTO thing_characterprofession
(character_id, profession_id, skill_type, current_skill, max_skill, recipes)
VALUES
($1, $2, $3, $4, $5, '%s')
`, recipes), character.Id, prof.Id, skillType, prof.Rank, prof.Max)
		if err != nil {
			cw.LogError(err, character)
			return err
		}
	} else if oldProf.Rank != prof.Rank || oldProf.Max != prof.Max || oldProf.RecipeHash != recipeHash {
		// Exists, update row
		_, err := db.Exec(fmt.Sprintf(`
UPDATE thing_characterprofession
SET current_skill = $1,
    max_skill = $2,
    recipes = '%s'
WHERE character_id = $3
  AND profession_id = $4
`, recipes), prof.Rank, prof.Max, character.Id, prof.Id)
		if err != nil {
			cw.LogError(err, character)
			return err
		}
	}

	return nil
}

func (cw *CharacterWorker) updateReputations(character *Character, cData *CharacterData) error {
	// Get a Redis connection
	conn := redisPool.Get()
	defer conn.Close()

	// Fetch valid factions
	rows, err := db.Query(`
SELECT id
FROM core_faction`)
	if err != nil {
		cw.LogError(err, character)
		return err
	}
	defer rows.Close()

	factionMap := make(map[int]bool)
	for rows.Next() {
		var factionId int
		err := rows.Scan(&factionId)
		if err != nil {
			cw.LogError(err, character)
			return err
		}

		factionMap[factionId] = true
	}

	// Build a faction map for this character
	rows, err = db.Query(`
SELECT faction_id,
       level,
       current,
       max_value
FROM thing_characterreputation
WHERE character_id = $1
`, character.Id)
	if err != nil {
		cw.LogError(err, character)
		return err
	}
	defer rows.Close()

	repMap := make(map[int]ReputationData)
	for rows.Next() {
		rep := ReputationData{}
		err := rows.Scan(&rep.Id, &rep.Standing, &rep.Value, &rep.Max)
		if err != nil {
			cw.LogError(err, character)
			return err
		}

		repMap[rep.Id] = rep
	}

	// Iterate over the reputations
	madeChanges := false
	var parts []string
	for _, rep := range cData.Reputation {
		if skipReputations[rep.Id] == true {
			continue
		}
		if _, ok := factionMap[rep.Id]; !ok {
			log.Debugf("Char%d: reputation %d doesn't exist in core_reputation", cw.id, rep.Id)
			continue
		}

		oldRep, ok := repMap[rep.Id]
		if !ok {
			// Doesn't exist, insert
			madeChanges = true
			_, err := db.Exec(`
INSERT INTO thing_characterreputation
(character_id, faction_id, level, current, max_value, paragon_max, paragon_ready, paragon_value)
VALUES
($1, $2, $3, $4, $5, 0, TRUE, 0)
`, character.Id, rep.Id, rep.Standing, rep.Value, rep.Max)
			if err != nil {
				cw.LogError(err, character)
				return err
			}
		} else if oldRep.Standing != rep.Standing || oldRep.Value != rep.Value || oldRep.Max != rep.Max {
			parts = append(parts, fmt.Sprintf("(%d, %d, %d, %d)", rep.Id, rep.Standing, rep.Value, rep.Max))
		}
	}

	if len(parts) > 0 {
		madeChanges = true
		_, err := db.Exec(fmt.Sprintf(`
UPDATE thing_characterreputation AS cr
SET level = new.level,
    current = new.current,
    max_value = new.max_value
FROM (VALUES
	%s
) AS new(faction_id, level, current, max_value)
WHERE cr.faction_id = new.faction_id
  AND cr.character_id = %d
`, strings.Join(parts, ", "), character.Id))
		if err != nil {
			cw.LogError(err, character)
			return err
		}
	}

	// Set cache time in Redis
	if madeChanges && character.BnetAccountId > 0 {
		key := fmt.Sprintf("updated:%d", character.BnetAccountId)
		now := fmt.Sprintf("%v", time.Now().UTC().Unix())

		if _, err := conn.Do("HSET", key, "reputations", now); err != nil {
			cw.LogError(err, character)
			return err
		}
	}

	return nil
}
