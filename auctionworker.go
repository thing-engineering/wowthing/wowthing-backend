package main

//go:generate ffjson -noencoder $GOFILE

import (
	//"crypto/sha1"
	"encoding/json"
	"fmt"
	"github.com/garyburd/redigo/redis"
	//"github.com/pquerna/ffjson/ffjson"
	"strconv"
	"strings"
	"time"
)

const (
	API_AUCTION            = "https://%s.api.blizzard.com/wow/auction/data/%s?locale=en_US&access_token=%s"
	AUCTION_CHECK_INTERVAL = 10 * 60
)

var auctionRealmMap map[string]map[string]int

// ffjson: skip
type AuctionInfo struct {
	Files []AuctionInfoFile
}

// ffjson: skip
type AuctionInfoFile struct {
	URL          string
	LastModified int64
}
type AuctionData struct {
	Auctions []AuctionDataRow
}
type AuctionDataRow struct {
	Id       int `json:"auc"`
	ItemId   int `json:"item"`
	Quantity int
	Bid      int64
	Buyout   int64
	TimeLeft string

	Owner      string
	OwnerRealm string

	Context int
	Rand    int
	Seed    int

	PetBreedId   int
	PetQualityId int
	PetSpeciesId int
	PetLevel     int

	//Modifiers  []AuctionDataRowModifier
	BonusLists []AuctionDataRowBonusList
}
type AuctionDataRowModifier struct {
	Type  int
	Value int
}
type AuctionDataRowBonusList struct {
	BonusId int `json:"bonusListId"`
}

// Short version for returned database rows
type AuctionRow struct {
	Id        int64
	AuctionId int
	Bid       int64
	TimeLeft  int
}

// ffjson: skip
type AuctionWorker struct {
	throttle <-chan time.Time
	id       int
	achan    chan *AuctionRealm
}

func StartAuctionWorkers(throttle <-chan time.Time, achan chan *AuctionRealm) {
	auctionRealmMap = make(map[string]map[string]int)

	for i := 1; i <= Config.General.AuctionWorkers; i++ {
		worker := &AuctionWorker{
			throttle,
			i,
			achan,
		}
		go worker.run()
	}
}

func (aw *AuctionWorker) run() {
	// Add this worker to the wait group
	worker_waitgroup.Add(1)
	defer worker_waitgroup.Done()

	log.Infof("AuctionWorker %d started", aw.id)

	for realm := range aw.achan {
		<-aw.throttle

		data, err := aw.fetchAuctionInfo(realm)
		if err != nil {
			aw.LogError(err, realm)
			continue
		}
		if data == "" {
			continue
		}

		var info AuctionInfo
		if err = json.Unmarshal([]byte(data), &info); err != nil {
			aw.LogError(err, realm)
			continue
		}

		// Sigh, Blizzard.
		if len(info.Files) == 0 {
			continue
		}

		var aData *AuctionData
		if aData, err = aw.fetchAuctionData(realm, info.Files[0]); err != nil {
			aw.LogError(err, realm)
			continue
		}

		if aData != nil {
			aw.handleAuctionData(realm, aData)
		}

		data = ""
		aData = nil
	}
}

func (aw *AuctionWorker) LogError(err error, realm *AuctionRealm) {
	LogErrorN(err, 2, "Auction%d: %s/%s", aw.id, realm.Region, realm.Slug)
}

func (aw *AuctionWorker) getRealmMap(realm *AuctionRealm) (map[string]int, error) {
	if cachedMap, ok := auctionRealmMap[realm.Region]; ok {
		return cachedMap, nil
	}

	rows, err := db.Query(`
SELECT  id, name
FROM    core_realm
WHERE   region = $1
`, realm.Region)
	if err != nil {
		aw.LogError(err, realm)
		return nil, err
	}
	defer rows.Close()

	realmMap := make(map[string]int)
	for rows.Next() {
		var realmID int
		var realmName string
		if err := rows.Scan(&realmID, &realmName); err != nil {
			aw.LogError(err, realm)
			return nil, err
		}
		realmMap[realmName] = realmID
	}

	auctionRealmMap[realm.Region] = realmMap

	return realmMap, nil
}

func (aw *AuctionWorker) getAuctionMap(realm *AuctionRealm) (map[int]*AuctionRow, error) {
	rows, err := db.Query(`
SELECT  id, auction_id, bid, time_left
FROM    auctions_auction
WHERE   realm_id = $1
`, realm.Id)
	if err != nil {
		aw.LogError(err, realm)
		return nil, err
	}
	defer rows.Close()

	auctionMap := make(map[int]*AuctionRow)
	for rows.Next() {
		var row AuctionRow
		if err = rows.Scan(&row.Id, &row.AuctionId, &row.Bid, &row.TimeLeft); err != nil {
			aw.LogError(err, realm)
			return nil, err
		}
		auctionMap[row.AuctionId] = &row
	}

	return auctionMap, nil
}

func (aw *AuctionWorker) fetchAuctionInfo(realm *AuctionRealm) (string, error) {
	// Get a Redis connection
	conn := redisPool.Get()
	defer conn.Close()

	// Check if we should request auction info for this realm
	key := fmt.Sprintf("auctionCheck:%s-%s", realm.Region, realm.Slug)
	_, err := redis.String(conn.Do("SET", key, "walrus", "EX", AUCTION_CHECK_INTERVAL, "NX"))
	if err != nil {
		return "", nil
	}

	url := fmt.Sprintf(API_AUCTION, realm.Region, realm.Slug, accessToken)

	// Go fetch
	data, err := httpGetString(url, 0)
	if err != nil {
		return "", err
	}

	return data, nil
}

func (aw *AuctionWorker) fetchAuctionData(realm *AuctionRealm, info AuctionInfoFile) (*AuctionData, error) {
	// Get a Redis connection
	conn := redisPool.Get()
	defer conn.Close()

	// Check if we should request auction data for this realm
	key := fmt.Sprintf("auctionModified:%s-%s", realm.Region, realm.Slug)
	lastModified, err := redis.Int64(conn.Do("GET", key))
	if err != redis.ErrNil && err != nil {
		return nil, err
	}

	if info.LastModified <= lastModified {
		return nil, nil
	}
	conn.Do("SET", key, info.LastModified)

	// Go fetch
	t1 := time.Now()
	data, err := httpGetString(info.URL, 0)
	if err != nil {
		return nil, err
	}

	// Parse JSON
	t2 := time.Now()
	var aData AuctionData
	if err = json.Unmarshal([]byte(data), &aData); err != nil {
		aw.LogError(err, realm)
		return nil, err
	}

	log.Debugf("Auction%d: %s/%s -- HTTP: %s, JSON: %s -- %d entries", aw.id, realm.Region, realm.Slug, t2.Sub(t1), time.Since(t2), len(aData.Auctions))

	return &aData, nil
}

func (aw *AuctionWorker) handleAuctionData(realm *AuctionRealm, aData *AuctionData) error {
	t1 := time.Now()

	// Retrieve all realms for this region
	realmMap, err := aw.getRealmMap(realm)
	if err != nil {
		return err
	}

	// Retrieve all current auctions for this realm
	auctionMap, err := aw.getAuctionMap(realm)
	if err != nil {
		return err
	}

	// Add new names
	auctionNameMap, err := aw.updateNames(realm, aData)
	if err != nil {
		return err
	}

	// Feed the auction handler
	ah := &AuctionHandler{}
	seenMap := make(map[int]bool)
	for _, auctionDataRow := range aData.Auctions {
		// Get a realm_id for the owner's realm
		ownerRealmId, ok := realmMap[auctionDataRow.OwnerRealm]
		if !ok {
			ownerRealmId = realm.Id
		}

		ah.UpdateAuction(auctionDataRow, auctionMap[auctionDataRow.Id], realm.Id, ownerRealmId, auctionNameMap[auctionDataRow.Owner])
		seenMap[auctionDataRow.Id] = true
	}

	// Generate IDs to delete
	for auctionID, auctionRow := range auctionMap {
		if _, ok := seenMap[auctionID]; !ok {
			ah.DeleteIDs = append(ah.DeleteIDs, strconv.FormatInt(auctionRow.Id, 10))
		}
	}

	// Run the queries
	t2 := time.Now()
	ah.RunQueries()

	log.Debugf("Auction%d: %s/%s -- work: %s, queries: %s", aw.id, realm.Region, realm.Slug, t2.Sub(t1), time.Since(t2))

	return nil
}

func (aw *AuctionWorker) updateNames(realm *AuctionRealm, aData *AuctionData) (map[string]int, error) {
	// Get unique names from the auction data
	uniqueNameMap := make(map[string]bool)
	for _, auctionDataRow := range aData.Auctions {
		uniqueNameMap[auctionDataRow.Owner] = true
	}

	var nameList []string
	for name := range uniqueNameMap {
		nameList = append(nameList, fmt.Sprintf("('%s')", name))
	}

	// TODO: fix concurrency?
	_, err := db.Exec(fmt.Sprintf(`
WITH data(name) AS (
    VALUES
        %s
)
INSERT INTO auctions_charactername
(name)
SELECT name
FROM data d
WHERE NOT EXISTS (
    SELECT  1
    FROM    auctions_charactername cn
    WHERE   cn.name = d.name
)
`, strings.Join(nameList, ", ")))
	if err != nil {
		aw.LogError(err, realm)
		return nil, err
	}

	// Now retrieve every name we'll need
	rows, err := db.Query(fmt.Sprintf(`
SELECT  id, name
FROM    auctions_charactername
WHERE   name IN (%s)
`, strings.Join(nameList, ", ")))
	if err != nil {
		aw.LogError(err, realm)
		return nil, err
	}

	auctionNameMap := make(map[string]int)
	for rows.Next() {
		var characterId int
		var characterName string
		if err := rows.Scan(&characterId, &characterName); err != nil {
			aw.LogError(err, realm)
			return nil, err
		}
		auctionNameMap[characterName] = characterId
	}

	return auctionNameMap, nil
}
