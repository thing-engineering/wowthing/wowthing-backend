package main

import (
	"encoding/json"
	"github.com/garyburd/redigo/redis"
	"strings"
	"time"
)

const (
	API_OAUTH_TOKEN  = "https://us.battle.net/oauth/token"
	TOKEN_CACHE_TIME = 86000
)

type TokenData struct {
	AccessToken string `json:"access_token"`
}

func AccessTokenThing() {
	log.Info("Access Token Thing started")

	getAccessToken()
	for _ = range time.Tick(time.Duration(1) * time.Hour) {
		getAccessToken()
	}
}

func getAccessToken() {
	// Get a Redis connection
	conn := redisPool.Get()
	defer conn.Close()

	log.Info("Requesting API access token...")

	data, err := httpPostString(API_OAUTH_TOKEN, strings.NewReader("grant_type=client_credentials"), clientId, clientSecret)
	if err != nil {
		LogErrorN(err, 1, "getAccessToken")
		return
	}

	var tData TokenData
	if err = json.Unmarshal([]byte(data), &tData); err != nil {
		LogErrorN(err, 1, "getAccessToken")
		return
	}

	accessToken = tData.AccessToken

	_, err = redis.String(conn.Do("SET", "access_token", accessToken, "EX", CHARACTER_CACHE_TIME))
	if err != nil {
		return
	}

	log.Info("Acquired API access token")
}
