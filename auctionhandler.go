package main

import (
	"fmt"
	"strconv"
	"strings"
)

var timeLeftMap map[string]int = map[string]int{
	"SHORT":     0,
	"MEDIUM":    1,
	"LONG":      2,
	"VERY_LONG": 3,
}

type AuctionHandler struct {
	DeleteIDs []string
	Inserts   []string
	Updates   []string
}

func (ah *AuctionHandler) RunQueries() error {
	// Deletes
	if len(ah.DeleteIDs) > 0 {
		_, err := db.Exec(fmt.Sprintf(`
DELETE  FROM auctions_auction
WHERE   id IN (%s)
`, strings.Join(ah.DeleteIDs, ", ")))
		if err != nil {
			LogError(err, "RunQueries")
			return err
		}
	}
	ah.DeleteIDs = nil

	// Inserts
	if len(ah.Inserts) > 0 {
		_, err := db.Exec(fmt.Sprintf(`
INSERT INTO auctions_auction
(
	realm_id, auction_id, item_id, quantity, bid, buyout, time_left, context, rand, seed, bonus_list,
 	pet_breed_id, pet_quality_id, pet_species_id, pet_level, owner_realm_id, owner_id
)
VALUES
%s
`, strings.Join(ah.Inserts, ", ")))
		if err != nil {
			LogError(err, "RunQueries")
			return err
		}
	}
	ah.Inserts = nil

	// Updates
	if len(ah.Updates) > 0 {
		_, err := db.Exec(fmt.Sprintf(`
UPDATE  auctions_auction AS a
SET     bid = new.bid,
        time_left = new.time_left
FROM    (
	        VALUES %s
        ) AS new(realm_id, auction_id, bid, time_left)
WHERE   a.realm_id = new.realm_id
        AND a.auction_id = new.auction_id
`, strings.Join(ah.Updates, ", ")))
		if err != nil {
			LogError(err, "RunQueries")
			return err
		}
	}
	ah.Updates = nil

	return nil
}

func (ah *AuctionHandler) UpdateAuction(auction AuctionDataRow, oldAuction *AuctionRow, realmID int, ownerRealmID int, ownerID int) error {
	timeLeft := timeLeftMap[auction.TimeLeft]
	if oldAuction != nil {
		if oldAuction.Bid != auction.Bid || oldAuction.TimeLeft != timeLeft {
			ah.Updates = append(ah.Updates, fmt.Sprintf("(%d, %d, %d, %d)", realmID, auction.Id, auction.Bid, timeLeft))
		}
	} else {
		// Auction is missing, insert it
		var bonusList []string
		for _, bonus := range auction.BonusLists {
			bonusList = append(bonusList, strconv.Itoa(bonus.BonusId))
		}

		ah.Inserts = append(ah.Inserts, fmt.Sprintf("(%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, '{%s}', %d, %d, %d, %d, %d, %d)",
			realmID,
			auction.Id,
			auction.ItemId,
			auction.Quantity,
			auction.Bid,
			auction.Buyout,
			timeLeft,
			auction.Context,
			auction.Rand,
			auction.Seed,
			strings.Join(bonusList, ", "),
			auction.PetBreedId,
			auction.PetQualityId,
			auction.PetSpeciesId,
			auction.PetLevel,
			ownerRealmID,
			ownerID))
	}

	return nil
}
