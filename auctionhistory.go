package main

import (
	"database/sql"
	"time"
)

const (
	REALM_AGGREGATION_QUERY = `
INSERT INTO auctions_realmhistory
(timestamp_id, realm_id, item_id, pet_species_id, amount, min_buyout, max_buyout, mean_buyout, median_buyout)
SELECT  $1,
        realm_id,
        item_id,
        pet_species_id,
        COUNT(buyout) AS amount,
        MIN(buyout) AS min_buyout,
        MAX(buyout) AS max_buyout,
        CAST(AVG(buyout) AS bigint) AS mean_buyout,
        CAST(median(buyout) AS bigint) AS median_buyout
FROM    auctions_auction
WHERE   buyout > 0
GROUP BY realm_id, item_id, pet_species_id
`

	REGION_AGGREGATION_QUERY = `
INSERT INTO auctions_regionhistory
(timestamp_id, region, item_id, pet_species_id, amount, min_buyout, max_buyout, mean_buyout, median_buyout)
SELECT  $1,
        r.region,
        a.item_id,
        a.pet_species_id,
        COUNT(a.buyout) AS amount,
        MIN(a.buyout) AS min_buyout,
        MAX(a.buyout) AS max_buyout,
        CAST(AVG(a.buyout) AS bigint) AS mean_buyout,
        CAST(median(a.buyout) AS bigint) AS median_buyout
FROM    auctions_auction a
INNER JOIN core_realm r ON a.realm_id = r.id
WHERE   a.buyout > 0
GROUP BY r.region, a.item_id, a.pet_species_id
`
)

func AuctionHistoryWorker() {
	log.Info("AuctionHistoryWorker started")

	for {
		time.Sleep(UntilNextHour())
		AuctionHistoryUpdate()
	}
}

func AuctionHistoryUpdate() {
	log.Debug("AuctionHistoryUpdate starting")
	timestamp := time.Now().UTC().Format("2006-01-02 15:00:00 UTC")

	var rowId int
	err := db.QueryRow("SELECT id FROM auctions_historytimestamp WHERE timestamp = $1", timestamp).Scan(&rowId)
	if err == sql.ErrNoRows {
		err := db.QueryRow(`
INSERT INTO auctions_historytimestamp
(timestamp)
VALUES ($1)
RETURNING id
`, timestamp).Scan(&rowId)
		if err != nil {
			LogError(err, "AuctionHistoryUpdate")
			return
		}
	} else if err != nil {
		LogError(err, "AuctionHistoryUpdate")
		return
	}

	// Update realm history
	t1 := time.Now()
	/*_, err = db.Exec(REALM_AGGREGATION_QUERY, rowId)
	if err != nil {
		LogError(err, "AuctionHistoryUpdate")
		return
	}*/

	// Update region history
	t2 := time.Now()
	/*_, err = db.Exec(REGION_AGGREGATION_QUERY, rowId)
	if err != nil {
		LogError(err, "AuctionHistoryUpdate")
		return
	}*/

	log.Debugf("AuctionHistoryUpdate finished - realm: %s, region: %s", t2.Sub(t1), time.Since(t2))
}

func UntilNextHour() time.Duration {
	now := time.Now().UTC()
	min := (60 - now.Minute() - 1)
	sec := (60 - now.Second() - 1)
	nano := 1000000000 - now.Nanosecond()

	if min < 0 {
		min = 9
	}
	if sec < 0 {
		sec = 59
	}

	return (time.Duration(min) * time.Minute) + (time.Duration(sec) * time.Second) + (time.Duration(nano) * time.Nanosecond)
}
