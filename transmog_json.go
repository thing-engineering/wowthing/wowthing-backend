package main

import (
	"encoding/json"
	"io/ioutil"
)

func LoadTransmogJson() {
	log.Info("Loading transmog JSON...")

	// Load transmog data
	data, err := ioutil.ReadFile("transmog.json")
	if err != nil {
		panic(err)
	}

	var transmogData map[string]map[string][]Transmog
	err = json.Unmarshal(data, &transmogData)
	if err != nil {
		panic(err)
	}

	var itemIDs []int
	for _, thing := range transmogData {
		for _, transmogs := range thing {
			for _, transmog := range transmogs {
				for _, itemID := range transmog.Items {
					itemIDs = append(itemIDs, itemID)
				}
			}
		}
	}

	InsertItems(itemIDs)

	log.Info("Transmog JSON loaded.")
}

type Transmog struct {
	Name  string `json:"n"`
	Items []int  `json:"i"`
}
