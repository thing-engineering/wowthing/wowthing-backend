package main

import (
	"encoding/json"
	"fmt"
	"time"
)

const (
	URL_SPELL        = "https://www.wowdb.com/api/spell/%d"
	SPELL_CACHE_TIME = 7 * 24 * 60 * 60
)

type SpellData struct {
	Id   int
	Name string
	Icon string
}

type SpellWorker struct {
	throttle <-chan time.Time
	id       int
	schan    chan int
}

func StartSpellWorkers(throttle <-chan time.Time, schan chan int) {
	for i := 1; i <= Config.General.SpellWorkers; i++ {
		worker := &SpellWorker{
			throttle,
			i,
			schan,
		}
		go worker.run()
	}
}

func (sw *SpellWorker) run() {
	// Add this worker to the wait group
	worker_waitgroup.Add(1)
	defer worker_waitgroup.Done()

	log.Infof("SpellWorker %d started", sw.id)

	for spellId := range sw.schan {
		data, err := sw.fetchSpell(spellId)
		if err != nil {
			continue
		}

		var sData SpellData
		if err = json.Unmarshal([]byte(data[1:len(data)-1]), &sData); err != nil {
			continue
		}

		if err = sw.updateSpell(spellId, &sData); err != nil {
			continue
		}

	}
}

func (sw *SpellWorker) LogError(err error) {
	LogErrorN(err, 2, "Spell%d", sw.id)
}

func (sw *SpellWorker) fetchSpell(spellId int) (string, error) {
	// Build the URL
	url := fmt.Sprintf(URL_SPELL, spellId)

	t := time.Now()

	// Don't overrun our per-second limit
	<-sw.throttle

	// Go fetch
	data, err := httpGetString(url, 0)
	if err != nil {
		sw.LogError(err)
		return "", err
	}

	log.Debugf("Spell%d: Spell %d took %s", sw.id, spellId, time.Since(t))

	return data, nil
}

func (sw *SpellWorker) updateSpell(spellId int, sData *SpellData) error {
	// Update spell data
	if sData.Id == spellId {
		_, err := db.Exec(`
UPDATE core_spell
SET name = $2,
    icon = $3
WHERE id = $1
`, spellId, sData.Name, sData.Icon)
		if err != nil {
			sw.LogError(err)
			return err
		}
	} else {
		log.Errorf("updateSpell error: %d != %d", spellId, sData.Id)
	}

	return nil
}
