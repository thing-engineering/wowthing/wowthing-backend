package main

import (
	"encoding/json"
	// "encoding/xml"
	"fmt"
	// "github.com/garyburd/redigo/redis"
	"strconv"
	"strings"
	"time"
)

const (
	// URL_ITEM        = "http://www.wowhead.com/item=%s&xml"
	URL_ITEM        = "https://www.wowdb.com/api/item/%d"
	ITEM_CACHE_TIME = 7 * 24 * 60 * 60
)

type ItemData struct {
	Id        int
	Name      string
	Icon      string
	BindType  int
	Class     int
	Level     int
	Quality   int
	SellPrice int
	Set       int `json:"SetID"`
	Slot      int `json:"InventorySlot"`
	SubClass  int

	Scaling struct {
		ItemLevel []int
	}
	Upgrades []struct {
		ItemLevel int
	}
}

type ItemWorker struct {
	throttle <-chan time.Time
	id       int
	ichan    chan int
}

func StartItemWorkers(throttle <-chan time.Time, ichan chan int) {
	for i := 1; i <= Config.General.ItemWorkers; i++ {
		worker := &ItemWorker{
			throttle,
			i,
			ichan,
		}
		go worker.run()
	}
}

func (iw *ItemWorker) run() {
	// Add this worker to the wait group
	worker_waitgroup.Add(1)
	defer worker_waitgroup.Done()

	log.Infof("ItemWorker %d started", iw.id)

	for itemId := range iw.ichan {
		data, err := iw.fetchItem(itemId)
		if err != nil {
			continue
		}

		var iData ItemData
		if err = json.Unmarshal([]byte(data[1:len(data)-1]), &iData); err != nil {
			continue
		}

		if err = iw.updateItem(itemId, &iData); err != nil {
			continue
		}

	}
}

func (iw *ItemWorker) LogError(err error) {
	LogErrorN(err, 2, "Item%d", iw.id)
}

func (iw *ItemWorker) fetchItem(itemId int) (string, error) {
	// Build the URL
	//url := fmt.Sprintf(API_ITEM, itemId)
	url := fmt.Sprintf(URL_ITEM, itemId)

	t := time.Now()

	// Don't overrun our per-second limit
	<-iw.throttle

	// Go fetch
	data, err := httpGetString(url, 0)
	if err != nil {
		iw.LogError(err)
		return "", err
	}

	log.Debugf("Item%d: Item %d took %s", iw.id, itemId, time.Since(t))

	return data, nil
}

func (iw *ItemWorker) updateItem(itemId int, iData *ItemData) error {
	// Update item data
	if iData.Id == itemId {
		levelScaling := ""
		if iData.Scaling.ItemLevel != nil {
			var itemLevels []string
			for i := 0; i < len(iData.Scaling.ItemLevel); i++ {
				itemLevels = append(itemLevels, strconv.Itoa(iData.Scaling.ItemLevel[i]))
			}
			levelScaling = strings.Join(itemLevels, ",")
		}
		levelScaling = fmt.Sprintf("{%s}", levelScaling)

		upgrades := ""
		if len(iData.Upgrades) > 1 {
			var upgradeLevels []string
			for i := 0; i < len(iData.Upgrades); i++ {
				upgradeLevels = append(upgradeLevels, strconv.Itoa(iData.Upgrades[i].ItemLevel))
			}
			upgrades = strings.Join(upgradeLevels, ",")
		}
		upgrades = fmt.Sprintf("{%s}", upgrades)

		_, err := db.Exec(`
UPDATE core_item
SET name = $2,
    lname = $3,
    icon = $4,
    bind_type = $5,
    cls = $6,
    level = $7,
    level_scaling = $8,
    quality = $9,
    sell_price = $10,
    slot = $11,
    subcls = $12,
    upgrades = $13,
    item_set = $14
WHERE id = $1
`, itemId, iData.Name, strings.ToLower(iData.Name), iData.Icon, iData.BindType, iData.Class, iData.Level,
			levelScaling, iData.Quality, iData.SellPrice, iData.Slot, iData.SubClass, upgrades, iData.Set)
		if err != nil {
			iw.LogError(err)
			return err
		}
	} else {
		log.Errorf("updateItem error: %d != %d", itemId, iData.Id)
	}

	return nil
}
